            /*__________________________________________________#
            #  Life is a journey, and it's the experiences we   #
            #  have along the way that define us. To shy from   #
            #  the difficult path least traveled is a travesty, #
            #  for it's here that we find the greatest bounty.  #
            #                                                   #
            #               ~ Perpetual Form ~                  #
            #                                                   #
            #   For my father, who taught me the true meaning   #
            #                   of courage.                     #
            #___________________________________________________#
            # File Name    :    rpc.cpp                         #
            # Dependencies :    LIB:                            #
            #                   USR: rpc.h                      #
            # Toolchain    :    GNU G++ v6                      #
            # Compilation  :    make rpclib                     #
            #___________________________________________________#
            #    https://www.linkedin.com/in/saad-ur-rahman/    #
            #  Copyright Saad Ur Rahman, All rights reserved.   #
            #     Usage and source code is subject to the       #
            #           GNU Affero GPL v3.0 license.            #
            #___________________________________________________#
            #                                                   #
            #                Basic RPC Library.                 #
            #__________________________________________________*/



//- Include(s): User:
#include "Client.h"
#include "rpc.h"
#include "RPCLib.h"
#include "Server.h"

//- Global Variable(s):
Server *g_Server = nullptr;                     // <Server> to be initialized.
RPCLib::ServerCache *g_ServerCache = nullptr;   // Local cache of function to <Server> mappings.



            /*_________________________________________________ #
            #               Method Definitions                  #
            # _________________________________________________*/



/* rpcInit:
*   Opens a listening socket and then a persistent connection to the <Binder>.
* -Parameters:  bool showBanner
* -Returns:     int
* -Throws:      n/a
*/
int rpcInit(bool showBanner)
{
    // Check to ensure <Server> has not already been initialized.
    if (g_Server != nullptr)
    {
        return -1;
    }

    // Setup <Server>.
    try
    {
        g_Server = new Server(showBanner);
    }
    catch (std::invalid_argument& err)
    {
        // Environment variables not set.
        std::cerr << "[ ERROR:" << err.what() << std::endl;
        return -2;
    }

    // Open listening socket.
    if (g_Server->openListeningSock() < 0)
    {
        return -3;
    }

    // Open connection to <Binder>.
    if (g_Server->connectToBinder() < 0)
    {
        return -4;
    }


    // Success.
    return 0;
}



/* rpcRegister:
*   Sends registration request to <Binder> and sets up local DB for a specific function.
* -Parameters:  char* name, int* argTypes, skeleton f
* -Returns:     int
* -Throws:      n/a
*/
int rpcRegister(char* name,
                int* argTypes,
                skeleton f)
{
    // Checks to ensure <Server> is setup.
    if (g_Server == nullptr)
    {
        return -1;
    }


    // Variable Declaration(s):
    int retval = 0;                             // Return code of request.


    // Send <REGISTER_REQ> to <Server> and await response.
    retval = g_Server->registerReq(name, argTypes, f);

    // Return the status code of request.
    return retval;
}



/* rpcExecute:
*   Puts the <Server> into a state which starts accepting servicing requests. Call is blocking.
* -Parameters:  n/a
* -Returns:     int
* -Throws:      n/a
*/
int rpcExecute()
{
    // Checks to ensure <Server> is setup.
    if (g_Server == nullptr)
    {
        return -1;
    }


    // Variable Declaration(s):
    int retval = 0;                             // Return code of request.


    // Start <Server>s acceptor loop.
    retval = g_Server->acceptor();


    // Cleanup memory and return.
    delete g_Server;
    return retval;
}



/* rpcTerminate:
*   Sends <Terminate> signal to <Binder> and causes cascading termination of <Server>s.
* -Parameters:  n/a
* -Returns:     int
* -Throws:      n/a
*/
int rpcTerminate()
{
    //- Call out to <Binder> with <TERMINATE> request.
    try
    {
        // Create <Client> to call <Binder> and make call.
        Client callBinder;
        if (callBinder.terminateReq() < 0)
        {
            return -3;
        }

    }
    catch (std::invalid_argument& err)
    {
        // Environment variables not set.
        return -1;
    }
    catch (std::runtime_error& err)
    {
        // Could not contact <Binder> for <TERMINATE>.
        return -2;
    }

    return 0;
}



/* rpcCall:
*   Calls <Binder> for <Server> address and then <Server> to <EXECUTE> the function.
* -Parameters:  char* name, int* argTypes, void** args
* -Returns:     int
* -Throws:      n/a
*/
int rpcCall(char* name,
            int* argTypes,
            void** args)
{
    // Variable Declaration(s):
    RPCLib::LocQuery serverAddr;                // Address of <Server> servicing request.
    FnSig fnSignature;                          // Functions <signature>.
    int retval = 0;                             // Return code of remote executing function.

    // Generate functions signature.
    RPCLib::FnSigBuilder(name, argTypes, fnSignature);

    //- Call out to <Binder> to get servicing <Server>s address.
    try
    {
        // Create <Client> to call <Binder> and make call.
        Client callBinder;
        serverAddr = callBinder.locationReq(fnSignature);

        // Check for no servicing <Server>s.
        if (serverAddr.m_errno != 0)
        {
            return -3;
        }

    }
    catch (std::invalid_argument& err)
    {
        // Environment variables not set.
        return -1;
    }
    catch (std::runtime_error& err)
    {
        // Could not contact <Binder> for <locationReq>.
        return -2;
    }


    //- Call <Server> for <EXECUTE> request.
    try
    {
        // Create <Client> to call <Server> and make call.
        Client callServer(serverAddr.m_Hostname, serverAddr.m_Port);
        retval = callServer.executeReq(name, argTypes, args);

        // Function not registered with <Server>.
        if (retval == 1)
        {
            return -6;
        }

        // Error in executing remote function.
        if (retval < 0)
        {
            return -6 + retval;
        }

        return retval;
    }
    catch (std::invalid_argument& err)
    {
        // Unable to extract <Server> address.
        return -4;
    }
    catch (std::runtime_error& err)
    {
        // Could not contact <Server> for <executionReq>.
        return -5;
    }

    // Return code of remote executing function.
    return retval;
}



/* rpcCacheCall:
*   Check local cache for <Server>s servicing function or send request to <Binder> for <Server>s.
* -Parameters:  char* name, int* argTypes, void** args
* -Returns:     int
* -Throws:      n/a
*/
int rpcCacheCall(char* name,
                 int* argTypes,
                 void** args)
{
    // Check for <g_ServerCache> initialization and initialize if necessary.
    if (g_ServerCache == nullptr)
    {
        g_ServerCache = new RPCLib::ServerCache;
    }


    // Generate <FnSig>.
    FnSig fnSignature;
    RPCLib::FnSigBuilder(name, argTypes, fnSignature);


    // Make only one cache call.
    for (size_t refreshed = 0; refreshed <= 1;)
    {
        // Attempt to lookup servicing <Server> and make <rpcCall>.
        try
        {
            // Attempt to lookup function in local cache. Throws <std::out_of_range> on search miss.
            std::list<SrvSig>& cache = g_ServerCache->at(fnSignature);

            // Check to see if the <Server> list is empty and force a cache update.
            if (cache.empty())
            {
                throw std::out_of_range("Cache is empty.");
            }

            // Setup return value. Initially set to <Server> unavailable status.
            int retval = -4;

            // Iterate over <cache> attempting to connect.
            for (auto iter = cache.begin(); iter != cache.end(); )
            {
                //- Call cached <Server> for <EXECUTE> request.
                try
                {
                    // Create <Client> to call <Server> and make call.
                    Client callServer(iter->m_Hostname, iter->m_Port);
                    retval = callServer.executeReq(name, argTypes, args);

                    // Halt on successful remote execution.
                    if (retval == 0)
                    {
                        break;
                    }

                    // Function not registered with <Server>.
                    if (retval == 1)
                    {
                        retval = -4;
                    }

                    // Error in executing remote function.
                    if (retval < 0)
                    {
                        retval = -4 + retval;
                    }

                    // Return the status code of the remote execution.
                    //return retval;
                }
                catch (...)
                {
                    // <Server> isn't available.
                }


                // Store the stale <Server> iterator and increment <iter>.
                auto stale = iter;
                ++iter;

                // Remove stale <Server>.
                cache.erase(stale);
            }

            // Check to see if the Server was unavailable and it can be refreshed.
            if (retval < 0 && refreshed < 1)
            {
                throw std::out_of_range("Refreshing Servers.");
            }

            // Return the status code of the remote execution.
            return retval;

        }
        catch (std::out_of_range& err)
        {
            // Lookup failed, make call to <Binder> for cache.
            try
            {
                // Cache list to be inserted into Database.
                std::list<SrvSig> cache;

                // Create <Client> to call <Binder> and make call.
                Client callBinder;
                callBinder.cacheCall(fnSignature, cache);

                // Check to see if <Server>s were found.
                if (cache.empty())
                {
                    return -3;
                }
                // Otherwise insert into the database.
                else
                {
                    (*g_ServerCache)[fnSignature] = std::move(cache);
                }

                // Cache <refreshed>.
                ++refreshed;
            }
            catch (std::invalid_argument& err)
            {
                // Environment variables not set.
                return -1;
            }
            catch (std::runtime_error& err)
            {
                // Could not contact <Binder> for <locationReq>.
                return -2;
            }
        }
    }

    return -4;
}



/* rpcCacheClear:
*   Clears the local cache of <Server>s and sets the database to a <nullptr>.
* -Parameters:  n/a
* -Returns:     int
* -Throws:      n/a
*/
int rpcCacheClear()
{
    // Check for <g_ServerCache> initialization.
    if (g_ServerCache == nullptr)
    {
        return -1;
    }

    // Clear cache and set to <nullptr>.
    delete g_ServerCache;
    g_ServerCache = nullptr;
    return 0;
}
