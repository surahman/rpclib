            /*__________________________________________________#
            #  Life is a journey, and it's the experiences we   #
            #  have along the way that define us. To shy from   #
            #  the difficult path least traveled is a travesty, #
            #  for it's here that we find the greatest bounty.  #
            #                                                   #
            #               ~ Perpetual Form ~                  #
            #                                                   #
            #   For my father, who taught me the true meaning   #
            #                   of courage.                     #
            #___________________________________________________#
            # File Name    :    Database.h                      #
            # Dependencies :    LIB:                            #
            #                   USR: Database.cpp               #
            # Toolchain    :    GNU G++ v6                      #
            # Compilation  :    make rpclib                     #
            #___________________________________________________#
            #    https://www.linkedin.com/in/saad-ur-rahman/    #
            #  Copyright Saad Ur Rahman, All rights reserved.   #
            #     Usage and source code is subject to the       #
            #           GNU Affero GPL v3.0 license.            #
            #___________________________________________________#
            #                                                   #
            #            Basic Binder Database Class.           #
            #__________________________________________________*/



#ifndef _DATABASE_H_
#define _DATABASE_H_

//- Include(s): Library:
#include <cstddef>
#include <list>
#include <unordered_map>
#include <unordered_set>

//- Include(s): User:

//- Namespace Import(s):
using std::string;

//- Forward Declaration(s):
struct Record;
struct SrvSig;
struct SrvSigHash;
struct FnSig;
struct FnSigHash;

//- Alias(es):
typedef std::unordered_map<SrvSig,              //- <Server> Database: All servers servicing a specific function.
                           Record,
                           SrvSigHash>
                           DBase;

typedef std::list<Record*> SchedulingList;      //- List of all <Server>s and their service schedule.

typedef std::unordered_set<FnSig,               //- List of all <Server>s and their service schedule.
                           FnSigHash>
                           Fn_DBase;

typedef std::unordered_map<int,                 //- <Server> to socket mappings.
                           SrvSig>
                           Server_DBase;



            /*_________________________________________________ #
            #               Data Type Definitions               #
            # _________________________________________________*/



// Function Signature: Name and <list> of parameters.
struct FnSig
{
    //-- Data Member(s):
    string m_Name;                              // Function name.
    std::list<uint16_t> m_paramList;            // Parameter <list>, see documentation for details.

    //-- Operator(s):
    bool operator==(const FnSig &rhs) const   // For use in <Fn_DBase> on hash collision.
    {
        // Parameter count check.
        if (m_paramList.size() != rhs.m_paramList.size())
        {
            return false;
        }

        // Name check.
        if (m_Name != rhs.m_Name)
        {
            return false;
        }

        // Loop over lists to ensure they're equal.
        auto lhsIter = m_paramList.cbegin();
        auto rhsIter = rhs.m_paramList.cbegin();
        auto lhsEnd = m_paramList.cend();
        for (; lhsIter != lhsEnd; ++lhsIter, ++rhsIter)
        {
            if (*lhsIter != *rhsIter)
            {
                return false;
            }
        }

        // All checks passed.
        return true;
    }
};



// Hash function for <FuncSig> for use in <Fn_DBase>.
struct FnSigHash
{
    // Operator(s):
    std::size_t operator() (const FnSig& key) const
    {
        // Variable Declaration(s):
        std::hash<string> strHasher;            // Specialized <string> <hasher>.
        std::hash<uint32_t> intHasher;          // Specialized <uint32_t> <hasher>.
        size_t retval = 0;                      // Running <hash> value.

        // Hash the function name.
        retval += strHasher(key.m_Name);

        // Loop over all elements calculating the <hash>.
        for (auto iter = key.m_paramList.cbegin(); iter != key.m_paramList.cend(); ++iter)
        {

            //Equivalent to: retval = retval * 31 + intHasher(*iter), just faster.
            retval = ((retval << 5) - retval) + intHasher(*iter);
        }

        // Return completed <hash>.
        return retval;
    }
};



// <Server>s <hostname> and <port>.
struct SrvSig
{
    //-- Constructor(s)/Destructor(s):
    SrvSig(string hostname = "", uint16_t port = 0) : m_Hostname(hostname), m_Port(port)
    {
    }

    //-- Data Member(s):
    string m_Hostname;                          // <Server>s address.
    uint16_t m_Port;                            // <Server>s port number.

    //-- Operator(s):
    bool operator==(const SrvSig &rhs) const    // For use in <DBase> on hash collision.
    {
        // Check for match against <hostname> and <port>. Numbers are quicker to compare, do them first.
        if (m_Port == rhs.m_Port && m_Hostname == rhs.m_Hostname)
        {
            return true;
        }

        // All checks failed.
        return false;
    }
};



// Hash function for <ServerSig> for use in <SrvFn_DBase>.
struct SrvSigHash
{
    //-- Operator(s):
    std::size_t operator() (const SrvSig& key) const
    {
        return std::hash<string>() (key.m_Hostname) ^
            (std::hash<uint16_t>() (key.m_Port) << 1);
    }
};



// Data for <Server> to function hash table.
struct Record
{
    //-- Constructor(s)/Destructor(s):
    Record(string hostname = "", uint16_t port = 0) : m_Server(SrvSig(hostname, port))
    {
    }

    Record(SrvSig &address) : m_Server(address)
    {
    }

    //-- Data Member(s):
    SrvSig m_Server;                            // <hostname> and <port> of <Server>.
    Fn_DBase m_FnDBase;                         // Database of all functions served by this <Server>.
    SchedulingList::const_iterator m_SchedPos;  // Iterator to position in <scheduler>, used for quick lookup.
};



            /*_________________________________________________ #
            #               Object Definitions                  #
            # _________________________________________________*/



class Database {
public:
    //-- Constructor(s)/Destructor(s):
    Database();                                 // Default constructor.
    ~Database();                                // Destructor.

    //-- Method(s):
                               
    int insert(SrvSig& address,                 //- Insert servicing data into DBase. <move> destructive on parameters.
               FnSig& signature,
               int fd_socket);

    int remove(int fd_Socket);                  // Removes the <Server> on <fd_Socket> from service.
    
    SrvSig scheduler(FnSig& signature);         // Locates a servicing <Server> and manages the scheduling.

    void findAll(FnSig& signature,              //- Find all <Server>s servicing <signature>.
                 std::list<SrvSig>& retval);

    std::pair<SchedulingList::const_iterator,   //- Checks to see if the function is already being serviced.
              bool>
              fnRegCheck(FnSig& signature) const;

    std::pair<Server_DBase::const_iterator,     //- Returns a constant iterator to the <Server>s.
              Server_DBase::const_iterator>
              getSrvs() const;

private:
    //-- Method(s):

    //-- Data Member(s):
    DBase m_DBase;                              // Database that contains all <Server>s keyed on <host> address.
    SchedulingList m_Scheduler;                 // Scheduling list of all <Server>s.
    Record k_SCHDBACK;                          // Tail of scheduler list.
    SchedulingList::const_iterator k_Sentinel;  // Iterator to sentinel value marking end of scheduler list.
    Server_DBase m_SrvDBase;                    // Database of all <Server>s keyed on their sockets.
};


#endif
