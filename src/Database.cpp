            /*__________________________________________________#
            #  Life is a journey, and it's the experiences we   #
            #  have along the way that define us. To shy from   #
            #  the difficult path least traveled is a travesty, #
            #  for it's here that we find the greatest bounty.  #
            #                                                   #
            #               ~ Perpetual Form ~                  #
            #                                                   #
            #   For my father, who taught me the true meaning   #
            #                   of courage.                     #
            #___________________________________________________#
            # File Name    :    Database.cpp                    #
            # Dependencies :    LIB:                            #
            #                   USR: Database.h                 #
            # Toolchain    :    GNU G++ v6                      #
            # Compilation  :    make rpclib                     #
            #___________________________________________________#
            #    https://www.linkedin.com/in/saad-ur-rahman/    #
            #  Copyright Saad Ur Rahman, All rights reserved.   #
            #     Usage and source code is subject to the       #
            #           GNU Affero GPL v3.0 license.            #
            #___________________________________________________#
            #                                                   #
            #            Basic Binder Database Class.           #
            #__________________________________________________*/



//- Include(s): Library:
#include <cassert>
#include <iostream>

//- Include(s): User:
#include "Database.h"



            /*_________________________________________________ #
            #               Method Definitions                  #
            # _________________________________________________*/



/* Binder:
*   Constructor:
* -Parameters:  string portStr = "0", int portNum = 0, int backLog = 10
* -Returns:     n/a
* -Throws:      n/a
*/
Database::Database() : k_SCHDBACK("_SENTINEL_", 0)
{
    // Reserve base <DB> size to reduce rehashing.
    m_DBase.reserve(128);

    // Setup spacer to mark end of scheduler.
    m_Scheduler.emplace_front(&k_SCHDBACK);
    k_Sentinel = m_Scheduler.begin();
}



/* ~Database:
*   Destructor:
* -Parameters:  n/a
* -Returns:     n/a
* -Throws:      n/a
*/
Database::~Database()
{
}



/* insert:
*   Inserts data into both the <Fn_DBase> and the <SrvFn_DBase>. <move> destructive on parameters.
* -Parameters:  SrvSig& address, FnSig& signature, int fd_socket
* -Returns:     int
* -Throws:      n/a
*/
int Database::insert(SrvSig& address,
                     FnSig& signature,
                     int fd_socket)
{
    // Variable Declaration(s):
    int retval = 0;                             // Return code.

    // Attempt to insert into <m_SrvDBase>.
    m_SrvDBase.insert(std::make_pair(fd_socket, address)).second;

    // Attempt to insert the host into <m_DBase>.
    auto DB_Iter = m_DBase.insert(std::make_pair(std::move(address), Record(address)));

    // Create a more readable alias for the <Record>.
    Record& record = DB_Iter.first->second;

    // If it was inserted it's a new <Server>, add it to the scheduler.
    if (DB_Iter.second == true)
    {
        record.m_SchedPos = m_Scheduler.insert(k_Sentinel, &record);
    }
    else
    {
        retval = 1;
    }

    // Move new signature to the <Server> if possible.
    if (!record.m_FnDBase.emplace(std::move(signature)).second)
    {
        retval = -2;
    }


    // Success.
    return retval;
}



/* remove:
*   Removes <Server> from service by removing it from the <Scheduler> and <DBase>.
* -Parameters:  int fd_Socket
* -Returns:     int
* -Throws:      n/a
*/
int Database::remove(int fd_Socket)
{
    // Get <Server> attached to a <fd_Socket>. Report fatal error if not found.
    auto address = m_SrvDBase.find(fd_Socket);
    if (address == m_SrvDBase.end())
    {
        return -1;
    }

    // Locate <address> in the <DBase>. Report fatal error if not found.
    auto DB_Iter = m_DBase.find(address->second);
    if (DB_Iter == m_DBase.end())
    {
        return -2;
    }

    // Remove from <Scheduler>.
    m_Scheduler.erase(DB_Iter->second.m_SchedPos);

    // Remove from <DBase>.
    m_DBase.erase(DB_Iter);

    // Remove from <SrvDBase>.
    m_SrvDBase.erase(address);

    // Move the <sentinel> to the back if necessary.
    if (k_Sentinel == m_Scheduler.cbegin())
    {
        m_Scheduler.splice(m_Scheduler.end(), m_Scheduler, k_Sentinel);
    }

    // Successful removal.
    return 0;
}



/* scheduler:
*   Locates servicing <Server> and moves it to the back of the <Scheduler>. Returns <0> port <Server> on failure.
* -Parameters:  FnSig& signature
* -Returns:     SrvSig
* -Throws:      n/a
*/
SrvSig Database::scheduler(FnSig& signature)
{
    // Search for a servicing <Server>.
    auto iter = fnRegCheck(signature);

    // Search hit: Move the <Server> to the back of the <Scheduler>, return the <SrvSig>.
    if (iter.second == true)
    {
        m_Scheduler.splice(m_Scheduler.end(), m_Scheduler, iter.first);
        return (*iter.first)->m_Server;
    }

    // Failed to find the <signature> and no <Server> is servicing the request.
    return (*k_Sentinel)->m_Server;
}



/* findAll:
*   Locate all <Server>s servicing a particular function.
* -Parameters:  FnSig& signature, std::list<SrvSig>& retval
* -Returns:     n/a
* -Throws:      n/a
*/
void Database::findAll(FnSig& signature,
                       std::list<SrvSig>& retval)
{
    // Iterate across the <SchedulingList> and locate ALL candidate <Server>s.
    for (auto iter = m_Scheduler.cbegin(); iter != m_Scheduler.cend(); ++iter)
    {
        // Skip the <sentinal> <Record>.
        if (iter == k_Sentinel)
        {
            continue;
        }

        // Look for <signature> in <Server>s servicing database.
        auto lookup = (*iter)->m_FnDBase.find(signature);

        // Search miss, restart the loop.
        if (lookup == (*iter)->m_FnDBase.cend())
        {
            continue;
        }

        // Search hit, add the <Server> to the list.
        retval.emplace_back((*iter)->m_Server);
    }

}



/* fnRegCheck:
*   Locate all <Server>s servicing a particular function.
* -Parameters:  FnSig& signature
* -Returns:     std::pair<SchedulingList::const_iterator, bool>
* -Throws:      n/a
*/
std::pair<SchedulingList::const_iterator, bool> Database::fnRegCheck(FnSig& signature) const
{
    // Variable Declaration(s):
    auto iter = m_Scheduler.begin();            // Iterator to servicing <Server>.

    // Iterate across the <SchedulingList> and locate first candidate <Server>.
    for ( ; iter != m_Scheduler.end(); ++iter)
    {
        // Skip the <sentinal> <Record>.
        if (iter == k_Sentinel)
        {
            continue;
        }

        // Look for <signature> in <Server>s servicing database.
        auto lookup = (*iter)->m_FnDBase.find(signature);

        // Search hit: Terminate the loop.
        if (lookup != (*iter)->m_FnDBase.end())
        {
            return std::make_pair(iter, true);
        }
    }

    // Search miss:
    return std::make_pair(iter, false);
}



/* getSrvs:
*   Returns constant iterator to the <Server>s.
* -Parameters:  n/a
* -Returns:     Server_DBase::const_iterator, Server_DBase::const_iterator
* -Throws:      n/a
*/
std::pair<Server_DBase::const_iterator, Server_DBase::const_iterator> Database::getSrvs() const
{
    return std::make_pair(m_SrvDBase.cbegin(), m_SrvDBase.cend());
}
