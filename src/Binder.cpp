            /*__________________________________________________#
            #  Life is a journey, and it's the experiences we   #
            #  have along the way that define us. To shy from   #
            #  the difficult path least traveled is a travesty, #
            #  for it's here that we find the greatest bounty.  #
            #                                                   #
            #               ~ Perpetual Form ~                  #
            #                                                   #
            #   For my father, who taught me the true meaning   #
            #                   of courage.                     #
            #___________________________________________________#
            # File Name    :    Binder.cpp                      #
            # Dependencies :    LIB:                            #
            #                   USR: Binder.h                   #
            # Toolchain    :    GNU G++ v6                      #
            # Compilation  :    make rpclib                     #
            #___________________________________________________#
            #    https://www.linkedin.com/in/saad-ur-rahman/    #
            #  Copyright Saad Ur Rahman, All rights reserved.   #
            #     Usage and source code is subject to the       #
            #           GNU Affero GPL v3.0 license.            #
            #___________________________________________________#
            #                                                   #
            #               Basic Binder Class.                 #
            #__________________________________________________*/



//- Include(s): Library:
#include <bitset>
#include <iostream>
#include <string.h>

//- Include(s): User:
#include "Binder.h"
#include "RPCLib.h"



            /*_________________________________________________ #
            #               Method Definitions                  #
            # _________________________________________________*/



/* Binder:
*   Constructor:
* -Parameters:  string portStr = "0", int portNum = 0, int backLog = 10
* -Returns:     n/a
* -Throws:      n/a
*/
Binder::Binder(string portStr,
               int portNum,
               int backLog) :
               m_PortServ(portNum),
               k_BackLog(backLog)
{
    // Display boot banner.
    RPCLib::banner(RPCLib::BannerType::BANNER_BINDER);

    // Bootstrap the listening socket.
    Bootstrap(portStr);

    // Start listening for inbound connections.
    if (listen(fd_LstnSock, k_BackLog) == -1)
    {
        throw std::runtime_error(" Error listening on server socket.]");
    }

    // Run <select> loop to multiplex socket connections.
    selector(fd_LstnSock);
}



/* ~Binder:
*   Destructor:
* -Parameters:  n/a
* -Returns:     n/a
* -Throws:      n/a
*/
Binder::~Binder()
{
}



/* Bootstrap:
*   Setup <Server>s listening socket.
* -Parameters:  string portNum
* -Returns:     n/a
* -Throws:      runtime_error
*/
void Binder::Bootstrap(string portNum)
{
    // Variable Declaration(s):
    struct addrinfo hints;                      // Hints to help setup socket.
    struct addrinfo *servinfo;                  // Server's listening socket info.
    struct addrinfo *lookup;                    // Address to bind socket too.
    int rv;                                     // Return code for <getaddrinfo>.
    int yes = 1;                                // Yes integer code.


    // Setup parameters for socket search.
    memset(&hints, 0, sizeof hints);            // Zero out <hints>.
    hints.ai_family = AF_UNSPEC;                // Either IPv4/IPv6.
    hints.ai_socktype = SOCK_STREAM;            // TCP/IP socket.
    hints.ai_flags = AI_PASSIVE;                // Local IP address.


                                                // Reverse DNS and assign to a host.
    if ((rv = getaddrinfo(NULL, portNum.c_str(), &hints, &servinfo)) != 0)
    {
        string err = " <getaddrinfo> failed: " + string(gai_strerror(rv)) + "]";
        throw std::runtime_error(err);
    }


    // <Lookup> address and bind to the first available and valid one.
    for (lookup = servinfo; lookup != NULL; lookup = lookup->ai_next)
    {
        // Attempt to bind to correct socket protocol (TCP/IP or UDP).
        if ((fd_LstnSock = socket(lookup->ai_family, lookup->ai_socktype, lookup->ai_protocol)) == -1)
        {
            continue;
        }

        // Setup the socket so it can be reused immediately after it's closed.
        if (setsockopt(fd_LstnSock, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1)
        {
            freeaddrinfo(servinfo);
            throw std::runtime_error(" Error setting <reuse> flag on server socket during binding.]");
        }

        // Bind the socket to the address.
        if (bind(fd_LstnSock, lookup->ai_addr, lookup->ai_addrlen) == -1)
        {
            close(fd_LstnSock);
            continue;
        }

        // Bind succeeded, terminate loop.
        break;
    }


    // Housekeeping and check to ensure bind succeeded.
    freeaddrinfo(servinfo);
    if (lookup == NULL)
    {
        throw std::runtime_error(" Failed to bind server socket.]");
    }


    // Get the socket port and hostname. Update <fd_LstnSock> and <m_Hostname>.
    std::pair<string, int> addr = RPCLib::GetAddr(fd_LstnSock, RPCLib::MsgType::SERVER);
    m_Hostname = addr.first;
    m_PortServ = addr.second;
    std::cout << "BINDER_ADDRESS " << m_Hostname;
    std::cerr << " \t - For local execution use the loopback hostname: "
              << "\"localhost\"." << std::endl;
    std::cout << "BINDER_PORT    " << m_PortServ << std::endl;

    std::cerr << "\n\nBash environment variable setup:\n"
              << "export BINDER_ADDRESS=" << m_Hostname << std::endl
              << "export BINDER_PORT=" << m_PortServ << std::endl;
}



/* selector:
*   Uses the <select> syscall to multiplex listening on a single socket.
* -Parameters:  int fd_Socket
* -Returns:     n/a
* -Throws:      runtime_error
*/
void Binder::selector(int fd_Socket)
{
    // Variable Declaration(s):
    fd_set fd_ReadSet;                          // Set of sockets to read from.
    int maxFD = fd_LstnSock;                    // Maximum file descriptor number.

    // Setup the <FD> sets.
    FD_ZERO(&fd_ReadSet);
    FD_ZERO(&fd_Collective);
    FD_SET(fd_Socket, &fd_Collective);


    while (true)
    {
        // Select is destructive so give it a copy <FD> set.
        fd_ReadSet = fd_Collective;

        // Terminate condition: No active connections and status is active.
        if (m_ConnCnt == 0 && !m_Active) { break; }

        // Select the socket when it's ready to read from.
        if (select(maxFD + 1, &fd_ReadSet, nullptr, nullptr, nullptr) < 0)
        {
            throw std::runtime_error(" <selector> encountered failure on select.]");
        }

        // Accept new <Client> connections.
        if (FD_ISSET(fd_Socket, &fd_ReadSet))
        {
            //-- Setup new socket.
            // Variable Declaration(s):
            int fd_Inbound;                         // Inbound socket file descriptor.
            struct sockaddr_storage clientAddr;     // <Client>'s address and socket information.
            socklen_t sin_size = sizeof clientAddr; // Size of the <clientAddr>.

            // Accept new inbound connection and add it to the <selector>.
            if ((fd_Inbound = accept(fd_LstnSock, (struct sockaddr *) &clientAddr, &sin_size)) < 0)
            {
                close(fd_Inbound);
                continue;
            }

            // Setup the <FD_SET> and FD numeric limit.
            FD_SET(fd_Inbound, &fd_Collective);
            fd_Inbound > maxFD ? maxFD = fd_Inbound : maxFD;

            // Increment the active connections.
            ++m_ConnCnt;
        }
        // Read form the connected sockets for data.
        else
        {
            // Variable Declaration(s):
            int fd_Active = 0;                  // Active connection sending data.
            uint8_t reqType = -1;               // Inbound request message type.

            // Find the set <FD> in the set.
            for (; fd_Active <= maxFD; ++fd_Active)
            {
                if (FD_ISSET(fd_Active, &fd_ReadSet))
                {
                    break;
                }
            }

            // Get request type and deal with terminated connection by removing from the set and closing socket.
            if (RPCLib::receiver(fd_Active, &reqType, RPCLib::k_MsgTypeBytes) <= 0)
            {
                // Remove the FD from the <FD_SET>.
                FD_CLR(fd_Active, &fd_Collective);
                close(fd_Active);

                // Remove the <Server> from the <DBase>.
                m_DBase.remove(fd_Active);

                // Decrement the active connections and restart loop.
                --m_ConnCnt;
                continue;
            }

            // Deal with request:
            switch (reqType)
            {
                case RPCLib::MsgType::REGISTER:
                    registerFn(fd_Active);
                    break;
                case RPCLib::MsgType::LOC_REQUEST:
                    locationReq(fd_Active);
                    break;
                case RPCLib::MsgType::TERMINATE:
                    terminate();
                    break;
                case RPCLib::MsgType::CACHE_CALL:
                    cacheCall(fd_Active);
                    break;
                default:
                    // Invalid request, shutdown <Client> connection.
                    FD_CLR(fd_Active, &fd_Collective);
                    close(fd_Active);
                    break;
            }

        }
    }
}



/* getFnSig:
*   Pulls function signature off socket.
* -Parameters:  int fd_Socket, FnSig& signature
* -Returns:     int
* -Throws:      n/a
*/
int Binder::getFnSig(int fd_Socket,
                     FnSig& signature)
{
    // Variable Declaration(s):
    uint32_t argCnt = 0;                        // <Message> length.
    char fnName[RPCLib::k_StringLength];        // Function name.


    // Get <argTypes> count.
    if (RPCLib::receiver(fd_Socket, &argCnt, sizeof(uint32_t)) < 0)
    {
        return -1;
    }
    argCnt = ntohl(argCnt);

    // Get function name.
    memset(fnName, 0, RPCLib::k_StringBytes);
    if (RPCLib::receiver(fd_Socket, &fnName, RPCLib::k_StringBytes) < 0)
    {
        return -1;
    }
    signature.m_Name = fnName;

    // Loop over arguments pulling them off the socket.
    for (size_t i = 0; i < argCnt; ++i)
    {
        // Get data off socket.
        uint16_t inbound = 0;
        if (RPCLib::receiver(fd_Socket, &inbound, RPCLib::k_argTypeBinderBytes) < 0)
        {
            return -1;
        }

        // Store in <signature>.
        inbound = ntohs(inbound);
        signature.m_paramList.emplace_back(inbound);
    }

    // Success.
    return 0;
}



/* getSrvSig:
*   Pulls function signature off socket.
* -Parameters:  int fd_Socket, SrvSig& signature
* -Returns:     int
* -Throws:      n/a
*/
int Binder::getSrvSig(int fd_Socket,
                      SrvSig& signature)
{
    // Variable Declaration(s):
    char hostname[RPCLib::k_StringLength];      // <Server> hostname.
    uint16_t port = 0;                          // <Server> port.


    // Get hostname.
    if (RPCLib::receiver(fd_Socket, &hostname, RPCLib::k_StringBytes) < 0)
    {
        return -1;
    }
    signature.m_Hostname = hostname;

    // Get <port>.
    if (RPCLib::receiver(fd_Socket, &port, RPCLib::k_PortBytes) < 0)
    {
        return -2;
    }
    port = ntohs(port);
    signature.m_Port = port;

    // Success.
    return 0;
}



/* registerFn:
*   Register a single function and make the appropriate entry in the DBases.
* -Parameters:  int fd_Socket
* -Returns:     int
* -Throws:      n/a
*/
int Binder::registerFn(int fd_Socket)
{
    // FORMAT: <REGISTER> <HOSTNAME> <PORT> <LENGTH> <FNNAME> <ARGTYPES>

    // Variable Declaration(s):
    SrvSig srvSignature;                        // <Server> signature.
    FnSig fnSignature;                          // Function signature.
    int32_t code = 0;                           // Code for registration.
                                                //- Registration status code.
    uint8_t reqType = (uint8_t)RPCLib::MsgType::REGISTER_SUCCESS;

    // Get <Server> signature.
    if (getSrvSig(fd_Socket, srvSignature) < 0)
    {
        // Send FAILURE message and code.
        reqType = (uint8_t)RPCLib::MsgType::REGISTER_FAILURE;
        RPCLib::sender(fd_Socket, &reqType, RPCLib::k_MsgTypeBytes);
        code = htonl(-1);
        RPCLib::sender(fd_Socket, &code, RPCLib::k_ReasonCodeBytes);

        return -1;
    }

    // Get function signature.
    if (getFnSig(fd_Socket, fnSignature) < 0)
    {
        // Send FAILURE message and code.
        reqType = (uint8_t)RPCLib::MsgType::REGISTER_FAILURE;
        RPCLib::sender(fd_Socket, &reqType, RPCLib::k_MsgTypeBytes);
        code = htonl(-1);
        RPCLib::sender(fd_Socket, &code, RPCLib::k_ReasonCodeBytes);

        return -2;
    }

    // Check to see if function has already been registered.
    auto lookup = m_DBase.fnRegCheck(fnSignature);
    if (lookup.second == true)
    {
        code = 1;
    }

    // Add function to <DBase> and link to <Server>.
    m_DBase.insert(srvSignature, fnSignature, fd_Socket);

    // Send SUCCESS message back to <Server>.
    reqType = (uint8_t)RPCLib::MsgType::REGISTER_SUCCESS;
    code = htonl(code);
    RPCLib::sender(fd_Socket, &reqType, RPCLib::k_MsgTypeBytes);
    RPCLib::sender(fd_Socket, &code, RPCLib::k_ReasonCodeBytes);

    // Success.
    return 0;
}



/* locationReq:
*   Location request for <Server> servicing a function.
* -Parameters:  int fd_Socket
* -Returns:     n/a
* -Throws:      n/a
*/
void Binder::locationReq(int fd_Socket)
{
    // FORMAT: <LOC_REQUEST> <LENGTH> <FNNAME> <ARGTYPES>

    // Variable Declaration(s):
    FnSig signature;                            // Function <signature> to lookup.
    uint8_t reqType = -1;                       // Request message.


    // Get function signature.
    getFnSig(fd_Socket, signature);

    // Lookup <Server> address.
    SrvSig srvAddr = m_DBase.scheduler(signature);

    // Search miss:
    if (srvAddr.m_Port == 0)
    {
        // Send <MsgType>.
        reqType = (uint8_t) RPCLib::MsgType::LOC_FAILURE;
        RPCLib::sender(fd_Socket, &reqType, RPCLib::k_MsgTypeBytes);

        // Send <reasonCode>:
        int32_t reasonCode = htonl(-1);
        RPCLib::sender(fd_Socket, &reasonCode, RPCLib::k_ReasonCodeBytes);
    }
    // Search hit:
    else
    {
        // Send <MsgType>.
        reqType = (uint8_t)RPCLib::MsgType::LOC_SUCCESS;
        RPCLib::sender(fd_Socket, &reqType, RPCLib::k_MsgTypeBytes);

        // Send <hostname>.
        char hostname[RPCLib::k_StringLength];
        memset(hostname, 0, RPCLib::k_StringBytes);
        srvAddr.m_Hostname.copy(hostname, srvAddr.m_Hostname.length());
        RPCLib::sender(fd_Socket, &hostname, RPCLib::k_StringBytes);

        // Send <port>.
        uint16_t port = htons(srvAddr.m_Port);
        RPCLib::sender(fd_Socket, &port, RPCLib::k_PortBytes);
    }
}



/* terminate:
*   Shutdown <Server>s and then the <Binder>.
* -Parameters:  n/a
* -Returns:     n/a
* -Throws:      n/a
*/
void Binder::terminate()
{
    // Variable Declaration(s):
    auto iter_range = m_DBase.getSrvs();        // Iterator range for <Server>s.

    // Set inactive status of <Binder> and close listening socket.
    m_Active = false;
    FD_CLR(fd_LstnSock, &fd_Collective);
    close(fd_LstnSock);

    // Loop over <Server>s and inform them of shutdown.
    for (auto iter = iter_range.first; iter != iter_range.second; ++iter)
    {
        // Skip over <Sentinel> place holder.
        if (iter->second.m_Port == 0)
        {
            continue;
        }

        // Close connection to <Server> causing it to terminate.
        FD_CLR(iter->first, &fd_Collective);
        close(iter->first);
        --m_ConnCnt;
    }
}



/* cacheCall:
*   Scrapes the database for all <Server>s servicing a function.
* -Parameters:  int fd_Socket
* -Returns:     n/a
* -Throws:      n/a
*/
void Binder::cacheCall(int fd_Socket)
{
    // Variable Declaration(s):
    std::list<SrvSig> cache;                    // List of all <Server>s servicing a request.
    FnSig signature;                            // Function signature to lookup in database.
    uint8_t reqType = -1;                       // Request message.

    // Get the function signature.
    getFnSig(fd_Socket, signature);

    // Scrape the DB for servicing <Server>s.
    m_DBase.findAll(signature, cache);

    // Search miss.
    if (cache.empty())
    {
        // Send: Message type.
        reqType = (uint8_t)RPCLib::MsgType::CACHE_FAILURE;
        RPCLib::sender(fd_Socket, &reqType, RPCLib::k_MsgTypeBytes);
    }
    // Search hit.
    else
    {
        // Send: Message type.
        reqType = (uint8_t)RPCLib::MsgType::CACHE_SUCCESS;
        if (RPCLib::sender(fd_Socket, &reqType, RPCLib::k_MsgTypeBytes) <= 0)
        {
            return;
        }

        // Send: <Server> count.
        uint32_t count = htonl(cache.size());
        if (RPCLib::sender(fd_Socket, &count, RPCLib::k_LengthBytes) <= 0)
        {
            return;
        }

        // Send: <Hostname> <Port>
        for (auto iter = cache.cbegin(); iter != cache.cend(); ++iter)
        {
            // Send <hostname>.
            char hostname[RPCLib::k_StringLength];
            memset(hostname, 0, RPCLib::k_StringBytes);
            iter->m_Hostname.copy(hostname, iter->m_Hostname.length());
            RPCLib::sender(fd_Socket, &hostname, RPCLib::k_StringBytes);

            // Send <port>.
            uint16_t port = htons(iter->m_Port);
            RPCLib::sender(fd_Socket, &port, RPCLib::k_PortBytes);
        }
    }
}
