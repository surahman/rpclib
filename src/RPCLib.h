            /*__________________________________________________#
            #  Life is a journey, and it's the experiences we   #
            #  have along the way that define us. To shy from   #
            #  the difficult path least traveled is a travesty, #
            #  for it's here that we find the greatest bounty.  #
            #                                                   #
            #               ~ Perpetual Form ~                  #
            #                                                   #
            #   For my father, who taught me the true meaning   #
            #                   of courage.                     #
            #___________________________________________________#
            # File Name    :    RPCLib.h                        #
            # Dependencies :    LIB:                            #
            #                   USR: RPCLib.cpp                 #
            # Toolchain    :    GNU G++ v6                      #
            # Compilation  :    make rpclib                     #
            #___________________________________________________#
            #    https://www.linkedin.com/in/saad-ur-rahman/    #
            #  Copyright Saad Ur Rahman, All rights reserved.   #
            #     Usage and source code is subject to the       #
            #           GNU Affero GPL v3.0 license.            #
            #___________________________________________________#
            #                                                   #
            #                Basic RPC Library.                 #
            #__________________________________________________*/



#ifndef _RPCLIB_H_
#define _RPCLIB_H_

//- Include(s): Library:
#include <cstddef>
#include <list>
#include <thread>
#include <utility>
//- Include(s): Library: Network:
#include <arpa/inet.h>
#include <iostream>
#include <netdb.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

//- Include(s): User:
#include "Database.h"



//- Scope all functions into namespace to avoid collisions.
namespace RPCLib {


            /*_________________________________________________ #
            #               Data Type Definitions               #
            # _________________________________________________*/




 //- Alias(es):
typedef int(*skeleton)(int *,                   // Skeleton function definition.
                       void **);

typedef std::unordered_map<FnSig,               //- <Server> cache for functions, local to client.
                           std::list<SrvSig>,
                           FnSigHash>
                           ServerCache;

/* Message type identifiers:
*   Identifiers count capped to 256 @ 1-byte per identifier.
*/
enum MsgType : uint8_t
{
    SERVER,                                     // Connection is from <Server>.
    CLIENT,                                     // Connection is from <Client>.
    REGISTER,                                   // <Server> registration request.
    REGISTER_SUCCESS,                           // <Server> registration request succeeded.
    REGISTER_FAILURE,                           // <Server> registration request failed.
    LOC_REQUEST,                                // <Client> location request.
    LOC_SUCCESS,                                // <Binder> indicating <Server> found.
    LOC_FAILURE,                                // <Binder> indicating <Server> found.
    EXECUTE,                                    // <Client> execute request to <Server>.
    EXECUTE_SUCCESS,                            // <Server> execute success to <Client>.
    EXECUTE_FAILURE,                            // <Server> execute failure to <Client>.
    TERMINATE,                                  // <Client> to <Binder> to <Server>s to shutdown.
    FAILURE,                                    // General <Failure> error.
    CACHE_CALL,                                 // <Client> sends a cache call to the <Binder>.
    CACHE_SUCCESS,                              // <Binder> found <Server>s servicing the request and returned list.
    CACHE_FAILURE,                              // <Binder> could not find <Server>s servicing the request.
};



/* Data type identifiers:
*   Data type bit count capped to 256 @ 1-byte per identifier.
*/
enum DataType : uint8_t
{
    ARG_CHAR = 1,                               // <char>.
    ARG_SHORT = 2,                              // <short>.
    ARG_INT = 3,                                // <int>.
    ARG_LONG = 4,                               // <long>.
    ARG_DOUBLE = 5,                             // <double>.
    ARG_FLOAT = 6,                              // <float>.
};



/* Banner type identifier:
*   Banner type bit count capped to 256 @ 1-byte per identifier.
*/
enum BannerType : uint8_t
{
    BANNER_NONE = 0,                            // Indicates no banner to be displayed.
    BANNER_BINDER = 1,                          // Indicates <Binder> banner to be displayed.
    BANNER_SERVER = 2,                          // Indicates <Server> banner to be displayed.
    BANNER_CLIENT = 3,                          // Indicates <Client> banner to be displayed.
};



// Specification type for each arguments passed in parameter.
struct ByteSpec
{
    //-- Constructor(s)/Destructor(s):
    ByteSpec(uint32_t size = 0,
             bool convert = false) :
             m_Size(size),
             m_Convert(convert)
    {
    }

    //-- Data Member(s):
    uint32_t m_Size;                            // Number of bytes in argument parameter.
    bool m_Convert;                             // Flag to indicate conversion requirement.
};



// Return type for a <LOC_Query> to binder.
struct LocQuery
{
    //-- Constructor(s)/Destructor(s):
    LocQuery(int16_t code = 0,
             RPCLib::MsgType type = RPCLib::MsgType::FAILURE,
             string hostname = "",
             uint16_t port = 0) :
             m_rspType(type),
             m_Hostname(hostname),
             m_Port(port), m_errno(code)
    {
    }

    //-- Data Member(s):
    RPCLib::MsgType m_rspType;                  // Response type.
    string m_Hostname;                          // <Server>s hostname.
    uint16_t m_Port;                            // <Server>s port number.
    int16_t m_errno;                            // Error code of request.
};



// Maximum character length of a string (hostname or function name).
const size_t k_StringLength = 64;

// <argType> single entry byte size for <Binder>.
const size_t k_argTypeBinderBytes = sizeof(uint16_t);

// <argType> single entry byte size.
const size_t k_argTypeBytes = sizeof(uint32_t);

// <Length> message
const size_t k_LengthBytes = sizeof(uint32_t);

// <Msg> type label size.
const size_t k_MsgTypeBytes = sizeof(uint8_t);

// Maximum size for a char string: hostname and function name.
const size_t k_StringBytes = k_StringLength * sizeof(char);

// Port number size.
const size_t k_PortBytes = sizeof(uint16_t);

// reasonCode size.
const size_t k_ReasonCodeBytes = sizeof(int32_t);

// Max number of threads a <Server> will spawn.
const size_t k_MaxThreads = 8;

// Number of threads the executing CPU supports.
const size_t k_HWThreads = std::thread::hardware_concurrency();

// Actual number of threads the <Server> will spawn.
const size_t k_NumThreads [[gnu::unused]] = std::min(k_HWThreads, k_MaxThreads);    // Marking as unused due to some
                                                                                    // compilers complaining.



            /*_________________________________________________ #
            #                Method Declarations                #
            # _________________________________________________*/


// Extracts hostname and port number from a socket file descriptor.
std::pair<string, int> GetAddr(int fd_Socket, RPCLib::MsgType type);



// Ensures that the complete <msg> is transmitted over <socket>.
int sender(int socket, const void* payload, size_t remaining);



// Receive a full message of <expectedSize> into <buffer>.
int receiver(int socket, void* buffer, size_t expectedSize);



// Packs the <Record> for transmission to the <Binder>.
void FnSigBuilder(const char* fnName, int* argTypes, FnSig& retval);



// Get <args> size from the <argTypes> array.
uint32_t argBytes(int* argTypes, std::list<RPCLib::ByteSpec> &byteSz);



// Converts <args> to network byte-order.
void byteOrderSwap(void *buffer, size_t length);



// Display boot banner.
void banner(RPCLib::BannerType process = RPCLib::BannerType::BANNER_NONE);

}



#endif
