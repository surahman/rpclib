            /*__________________________________________________#
            #  Life is a journey, and it's the experiences we   #
            #  have along the way that define us. To shy from   #
            #  the difficult path least traveled is a travesty, #
            #  for it's here that we find the greatest bounty.  #
            #                                                   #
            #               ~ Perpetual Form ~                  #
            #                                                   #
            #   For my father, who taught me the true meaning   #
            #                   of courage.                     #
            #___________________________________________________#
            # File Name    :    Server.cpp                      #
            # Dependencies :    LIB:                            #
            #                   USR: Server.h                   #
            # Toolchain    :    GNU G++ v6                      #
            # Compilation  :    make rpclib                     #
            #___________________________________________________#
            #    https://www.linkedin.com/in/saad-ur-rahman/    #
            #  Copyright Saad Ur Rahman, All rights reserved.   #
            #     Usage and source code is subject to the       #
            #           GNU Affero GPL v3.0 license.            #
            #___________________________________________________#
            #                                                   #
            #               Basic Server Class.                 #
            #__________________________________________________*/



//- Include(s): User:
#include "Server.h"



            /*_________________________________________________ #
            #               Method Definitions                  #
            # _________________________________________________*/



/* Server:
*   Constructor. Setup listening socket and starts accepting inbound connections.
* -Parameters:  bool showBanner, int portNum = 0, int backLog = 10
* -Returns:     n/a
* -Throws:      n/a
*/
Server::Server(bool showBanner,
               int portNum,
               int backLog) :
               k_BackLog(backLog),
               m_ServerAddr("", portNum)
{
    // If the <portNum> is not provided use <getenv> to get address.
    if (m_BinderAddr.m_Port == 0)
    {
        // Get <hostname>.
        char* host = secure_getenv("BINDER_ADDRESS");
        if (host == nullptr)
        {
            throw std::invalid_argument(" Please set the <Binder>s address Environment Variable.]");
        }
        m_BinderAddr.m_Hostname = host;

        // Get <port>.
        char* port = secure_getenv("BINDER_PORT");
        if (port == nullptr)
        {
            throw std::invalid_argument(" Please set the <Binder>s port Environment Variable.]");
        }

        // Convert to integer <port>.
        m_BinderAddr.m_Port = atoi(port);
    }

    // Display banner.
    if (showBanner)
    {
        RPCLib::banner(RPCLib::BannerType::BANNER_SERVER);
    }


}



/* ~Server:
*   Destructor: Closes the communication <port>, shuts down worker threads.
* -Parameters:  n/a
* -Returns:     n/a
* -Throws:      n/a
*/
Server::~Server()
{
    // Wait for the <terminateHandler> to insert sentinel values for sockets.
    if (trd_Binder != nullptr)
    {
        trd_Binder->join();
        delete trd_Binder;
        trd_Binder = nullptr;
    }

    // Wait for all threads to shutdown.
    for (auto iter = m_ThreadPool.begin(); iter != m_ThreadPool.end(); ++iter)
    {
        if (iter->joinable())
        {
            iter->join();
        }
    }

    // Clean up memory for <Client> which will in turn disconnect from the <Binder>.
    delete m_Binder;
}



/* openListeningSock():
*   Opens the <Server>s listening socket to accept inbound connections on.
* -Parameters:  n/a
* -Returns:     n/a
* -Throws:      n/a
*/
int Server::openListeningSock()
{
    // Bootstrap the listening socket.
    try
    {
        Bootstrap("0");
    }
    catch (...)
    {
        return -1;
    }

    // Start listening for inbound connections.
    if (listen(fd_LstnSock, k_BackLog) == -1)
    {
        return -2;
    }

    // Success.
    return 0;
}



/* connectToBinder:
*   Opens a connection to the <Binder>.
* -Parameters:  n/a
* -Returns:     int
* -Throws:      n/a
*/
int Server::connectToBinder()
{
    // Safety check to ensure the connection to <Binder> is singular.
    if (m_Binder != nullptr)
    {
        return -1;
    }

    // Attempt to setup <Client> to call <Binder>.
    try
    {
        m_Binder = new Client(m_BinderAddr.m_Hostname, m_BinderAddr.m_Port);
    }
    catch (...)
    {
        delete m_Binder;
        m_Binder = nullptr;
        return -2;
    }

    // Success.
    return 0;
}



/* Bootstrap:
*   Setup <Server>s listening socket.
* -Parameters:  string portNum
* -Returns:     n/a
* -Throws:      runtime_error
*/
void Server::Bootstrap(string portNum)
{
    // Variable Declaration(s):
    struct addrinfo hints;                      // Hints to help setup socket.
    struct addrinfo *servinfo;                  // Server's listening socket info.
    struct addrinfo *lookup;                    // Address to bind socket too.
    int rv;                                     // Return code for <getaddrinfo>.
    int yes = 1;                                // Yes integer code.


    // Setup parameters for socket search.
    memset(&hints, 0, sizeof hints);            // Zero out <hints>.
    hints.ai_family = AF_UNSPEC;                // Either IPv4/IPv6.
    hints.ai_socktype = SOCK_STREAM;            // TCP/IP socket.
    hints.ai_flags = AI_PASSIVE;                // Local IP address.


    // Reverse DNS and assign to a host.
    if ((rv = getaddrinfo(NULL, portNum.c_str(), &hints, &servinfo)) != 0)
    {
        string err = " <getaddrinfo> failed: " + string(gai_strerror(rv)) + "]";
        throw std::runtime_error(err);
    }


    // <Lookup> address and bind to the first available and valid one.
    for (lookup = servinfo; lookup != NULL; lookup = lookup->ai_next)
    {
        // Attempt to bind to correct socket protocol (TCP/IP or UDP).
        if ((fd_LstnSock = socket(lookup->ai_family, lookup->ai_socktype, lookup->ai_protocol)) == -1)
        {
            continue;
        }

        // Setup the socket so it can be reused immediately after it's closed.
        if (setsockopt(fd_LstnSock, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1)
        {
            freeaddrinfo(servinfo);
            throw std::runtime_error(" Error setting <reuse> flag on server socket during binding.]");
        }

        // Bind the socket to the address.
        if (bind(fd_LstnSock, lookup->ai_addr, lookup->ai_addrlen) == -1)
        {
            close(fd_LstnSock);
            continue;
        }

        // Bind succeeded, terminate loop.
        break;
    }


    // Housekeeping and check to ensure bind succeeded.
    freeaddrinfo(servinfo);
    if (lookup == NULL)
    {
        throw std::runtime_error(" Failed to bind server socket.]");
    }


    // Get the socket port and hostname. Update <fd_LstnSock> and <m_Hostname>.
    std::pair<string, int> addr =  GetAddr(fd_LstnSock, RPCLib::MsgType::SERVER);
    m_ServerAddr.m_Hostname = addr.first;
    m_ServerAddr.m_Port = addr.second;
    //std::cout << "SERVER_ADDRESS " << m_ServerAddr.m_Hostname << std::endl;
    //std::cout << "SERVER_PORT " << m_ServerAddr.m_Port << std::endl;
}



/* acceptor:
*   Acceptor loop that accepts new work requests.
* -Parameters:  n/a
* -Returns:     int
* -Throws:      n/a
*/
int Server::acceptor()
{
    // Check to ensure functions are registered for service.
    if (fn_DBase.empty())
    {
        return -2;
    }

    // Check to ensure the acceptor is only executed once.
    if (m_Active == true)
    {
        return -3;
    }
    m_Active = true;


    // Variable Declaration(s):
    int fd_Inbound = -1;                        // Inbound connections socket.
    struct sockaddr_storage clientAddr;         // <Client>'s address and socket information.
    socklen_t sin_size = sizeof clientAddr;     // Size of the <clientAddr>.


    // Launch the <terminateHandler> thread.
    trd_Binder = new std::thread(&Server::terminateHandler, this);

    // Setup thread pool.
    for (size_t i = 0; i < RPCLib::k_NumThreads; ++i)
    {
        m_ThreadPool.emplace_back(std::thread(&Server::requestHandler, this));
    }


    // Keep servicing whilst the <Server> is active.
    while (m_Active)
    {
        // Accept new inbound connection and add it to the <selector>.
        if ((fd_Inbound = accept(fd_LstnSock, (struct sockaddr *) &clientAddr, &sin_size)) < 0)
        {
            // Once the listening socket <fd_LstnSock> is closed <accept> will return <-1>, which is an error
            // code and not a file descriptor. Do not attempt to close invalid/reserved FD's, which are [< 3].
            if (fd_Inbound > 2)
            {
                close(fd_Inbound);
            }
            continue;
        }

        // Check job request type, discard anything but <EXECUTE>.
        uint8_t reqType = -1;
        RPCLib::receiver(fd_Inbound, &reqType, RPCLib::k_MsgTypeBytes);
        if (reqType != (uint8_t)RPCLib::MsgType::EXECUTE)
        {
            close(fd_Inbound);
            continue;
        }

        //---- CRITICAL SECTION BEGINS ----||
        {
            std::lock_guard<std::mutex> lock(lk_JobQueue);

            // Insert the socket into the <JobQueue>.
            m_JobQueue.push(fd_Inbound);

            // Signal to empty CV is lost, see documentation for rationale.
            cv_JobQueue.notify_one();
        }
        //----  CRITICAL SECTION ENDS  ----||

    }

    // Success.
    return 0;
}



/* requestHandler:
*   Worker that processes a work request.
* -Parameters:  n/a
* -Returns:     n/a
* -Throws:      n/a
*/
void Server::requestHandler()
{
    // Loop pulling job requests until terminate sentinel.
    for ( ; ; )
    {
        // Variable Declaration(s):
        int fd_JobRequest;                          // Socket with a job request.
        char fnName[64];                            // Function name.
        uint32_t argTypeCnt = 0;                    // <argType> count.
        std::list<RPCLib::ByteSpec> argsBytes;      // <args> byte size.
        int* argTypes = nullptr;                    // <argTypes> array.
        void** args = nullptr;                      // <args> data for parameters.



        //---- CRITICAL SECTION BEGINS ----||
        {
            std::unique_lock<std::mutex> lock(lk_JobQueue);

            // If the <Job Queue> is empty, wait for work.
            if (m_JobQueue.empty())
            {
                cv_JobQueue.wait(lock, [this] { return !m_JobQueue.empty(); });
            }

            // Get the socket from the <JobQueue>.
            fd_JobRequest = m_JobQueue.front();
            m_JobQueue.pop();

            // Check for terminate sentinel value <-1> and exit.
            if (fd_JobRequest == -1)
            {
                break;
            }
        }
        //----  CRITICAL SECTION ENDS  ----||

        // Get data off socket. On failure restart loop.
        if (getExecReq(fd_JobRequest, fnName, argTypeCnt, argTypes, argsBytes, args) < 0)
        {
            close(fd_JobRequest);
            continue;
        }

        // Execute function.
        execFn(fnName, argTypes, args, argsBytes, fd_JobRequest);


        // Clean up all memory.
        delete[] argTypes;
        for (size_t i = 0; i < argTypeCnt; ++i)
        {
            free(args[i]);
        }
        free(args);
    }

}



/* terminateHandler:
*   Places sentinel values in <m_JobQueue> to shutdown all worker threads.
* -Parameters:  n/a
* -Returns:     n/a
* -Throws:      n/a
*/
void Server::terminateHandler()
{
    // Non-busy wait for terminate from <Binder>.
    m_Binder->binderHeartBeat();

    // Shutdown listen socket and set <Server> to inactive.
    shutdown(fd_LstnSock, 2);
    close(fd_LstnSock);
    m_Active = false;


    //---- CRITICAL SECTION BEGINS ----||
    // Facilitate graceful exit by allowing all <Jobs> to complete.
    {
        std::unique_lock<std::mutex> lock(lk_JobQueue);

        // Place sentinel value of <-1> in <m_JobQueue>.
        for (size_t i = 0; i < RPCLib::k_NumThreads; ++i)
        {
            m_JobQueue.push(-1);
        }

        // Wake up all potentially waiting threads to shut them down.
        cv_JobQueue.notify_all();
    }
    //----  CRITICAL SECTION ENDS  ----||

}



/* registerReq:
*   Calls out to the <Binder> to register a function for service.
* -Parameters:  char* name, int* argTypes, RPCLib::skeleton f
* -Returns:     int
* -Throws:      n/a
*/
int Server::registerReq(char* name,
                        int* argTypes,
                        RPCLib::skeleton f)
{
    // Ensure <Client> is setup.
    if (m_Binder == nullptr)
    {
        return -1;
    }


    // Variable Declaration(s):
    uint32_t retval = 0;                        // Return code of function execution.


    // Package function <signature> as <16-bit> representation.
    FnSig signature;
    RPCLib::FnSigBuilder(name, argTypes, signature);

    // Check for duplicate registration attempts.
    lk_FNDBase.lock();
    auto lookup = fn_DBase.find(signature);
    lk_FNDBase.unlock();
    if (lookup != fn_DBase.end())
    {
        fn_DBase[std::move(signature)] = f;
        return 2;
    }

    // Send registration request to <Binder>.
    if ((retval = m_Binder->registrationReq(m_ServerAddr, signature)) < 0)
    {
        return -3;
    }

    // Add the <skeleton> to the database.
    lk_FNDBase.lock();
    fn_DBase.insert(std::make_pair(std::move(signature), f));
    lk_FNDBase.unlock();


    // Return status code of request.
    return retval;
}



/* getExecReq:
*   Get the complete <EXECUTE> request off the socket.
* -Parameters:  int fd_Socket, char* fnName, uint32_t& argCnt, int* &argTypes,
*               std::list<RPCLib::ByteSpec> &byteSize, void** &args
* -Returns:     int
* -Throws:      n/a
*/
int Server::getExecReq(int fd_Socket,
                       char* fnName,
                       uint32_t& argCnt,
                       int* &argTypes,
                       std::list<RPCLib::ByteSpec> &byteSize,
                       void** &args)
{
    //- Execute request: <EXECUTE> <fnName> <argCnt> <argTypes> <args>

    // Get <fnName>.
    if (RPCLib::receiver(fd_Socket, fnName, RPCLib::k_StringBytes) <= 0)
    {
        return -1;
    }


    // Get <argCnt>.
    if (RPCLib::receiver(fd_Socket, &argCnt, RPCLib::k_argTypeBytes) <= 0)
    {
        return -1;
    }
    argCnt = ntohl(argCnt);


    // Get <argTypes>.
    argTypes = new int[argCnt + 1];
    for (size_t i = 0; i < argCnt; ++i)
    {
        // Recover the <argType> from the socket.
        uint32_t buffer = 0;
        if (RPCLib::receiver(fd_Socket, &buffer, RPCLib::k_argTypeBytes) <= 0)
        {
            delete[] argTypes;
            return -1;
        }

        // Store the <argType>.
        argTypes[i] = ntohl(buffer);
    }
    argTypes[argCnt] = 0;


    // Extract argument byte size.
    RPCLib::argBytes(argTypes, byteSize);


    // Get <args>.
    args = (void **) malloc(argCnt * sizeof(void *));
    auto iter = byteSize.cbegin();
    for (int i = 0; i < (int) argCnt; ++i, ++iter) // Cast to <int> to stop compiler warnings.
    {
        // Allocate space for argument.
        args[i] = (void*) malloc(iter->m_Size);

        // Get bytes off socket.
        if (RPCLib::receiver(fd_Socket, args[i], iter->m_Size) <= 0)
        {
            // Cleanup memory and exit.
            for ( ; i >= 0; --i)
            {
                free(args[i]);
            }
            free(args);
            return -1;
        }

        // Convert to host byte order.
        if (iter->m_Convert == true)
        {
            RPCLib::byteOrderSwap(args[i], iter->m_Size);
        }
    }

    // Success.
    return 0;
}



/* execFn:
*   <EXECUTE>s the actual request and returns to the <Client>.
* -Parameters:  char* fnName, int* argTypes, void** args, std::list<uint32_t> &byteSize, int fd_Socket
* -Returns:     n/a
* -Throws:      n/a
*/
void Server::execFn(char* fnName,
                    int* argTypes,
                    void** args,
                    std::list<RPCLib::ByteSpec> &byteSize,
                    int fd_Socket)
{
    // Variable Declaration(s):
    FnSig signature;                            // Execution request functions <signature>.
    int32_t retval = 0;                         // Return code of function execution.
                                                //- Execution status code.
    uint8_t reqType = RPCLib::MsgType::EXECUTE_SUCCESS;

    // Prep and lookup <signature>.
    RPCLib::FnSigBuilder(fnName, argTypes, signature);
    auto lookup = fn_DBase.find(signature);

    // Check for unregistered function.
    if (lookup == fn_DBase.end())
    {
        reqType = RPCLib::MsgType::EXECUTE_FAILURE;
        RPCLib::sender(fd_Socket, &reqType, RPCLib::k_MsgTypeBytes);
        retval = htonl(1);
        RPCLib::sender(fd_Socket, &retval, RPCLib::k_ReasonCodeBytes);
        return;
    }

    // Execute function using <skeleton>.
    retval = (lookup->second)(argTypes, args);

    // Check for function execution failure.
    if (retval < 0)
    {
        reqType = RPCLib::MsgType::EXECUTE_FAILURE;
        RPCLib::sender(fd_Socket, &reqType, RPCLib::k_MsgTypeBytes);
        retval = htonl(retval);
        RPCLib::sender(fd_Socket, &retval, RPCLib::k_ReasonCodeBytes);
        return;
    }


    //- Send: <EXECUTE_SUCCESS> <args>
    RPCLib::sender(fd_Socket, &reqType, RPCLib::k_MsgTypeBytes);

    size_t argCnt = byteSize.size();
    auto iter = byteSize.begin();
    for (size_t i = 0; i < argCnt; ++i, ++iter)
    {
        // Switch to network byte order.
        if (iter->m_Convert == true)
        {
            RPCLib::byteOrderSwap(args[i], iter->m_Size);
        }

        // Send argument. Stop loop on send failure.
        if (RPCLib::sender(fd_Socket, args[i], iter->m_Size) <= 0)
        {
            break;
        }
    }

}
