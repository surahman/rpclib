            /*__________________________________________________#
            #  Life is a journey, and it's the experiences we   #
            #  have along the way that define us. To shy from   #
            #  the difficult path least traveled is a travesty, #
            #  for it's here that we find the greatest bounty.  #
            #                                                   #
            #               ~ Perpetual Form ~                  #
            #                                                   #
            #   For my father, who taught me the true meaning   #
            #                   of courage.                     #
            #___________________________________________________#
            # File Name    :    MainBinder.cpp                  #
            # Dependencies :    LIB:                            #
            #                   USR: Binder.h                   #
            # Toolchain    :    GNU G++ v6                      #
            # Compilation  :    make binder                     #
            #___________________________________________________#
            #    https://www.linkedin.com/in/saad-ur-rahman/    #
            #  Copyright Saad Ur Rahman, All rights reserved.   #
            #     Usage and source code is subject to the       #
            #           GNU Affero GPL v3.0 license.            #
            #___________________________________________________#
            #                                                   #
            #               Binder Entry Point.                 #
            #__________________________________________________*/



//- Include(s): Library:
#include <cstdlib>
#include <exception>
#include <iostream>

//- Include(s): User:
#include "Binder.h"

//- Namespace import(s):

//- Global Variable(s):



            /*_________________________________________________ #
            #                     [ MAIN ]:                     #
            # _________________________________________________*/


int main(int argc, char* argv[])
{
    // Variable Declaration(s):
    std::string portStr = "0";                  // Listening port number.
    int port = 0;                               // Listening port number.
    int backLog = 10;                           // Connection backlog counter.


    // Validate all parameters.
    try
    {
        // Port number provided, validate.
        if (argc > 1)
        {
            portStr = argv[1];
            port = atoi(argv[1]);
            if (port < 1024 || port > 65535)
            {
                throw std::invalid_argument(" Invalid port range, must be in range [1024 - 65535].]");
            }
        }

        // Validate connection <backLog> counter.
        if (argc > 2)
        {
            backLog = atoi(argv[2]);
            if (backLog < 1)
            {
                throw std::invalid_argument(" Invalid connection backlog count.]");
            }
        }

        //Setup <Server>.
        Binder server(portStr, port, backLog);

    }
    catch (std::exception &err)
    {
        std::cerr << "[" << argv[0] << err.what() << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
