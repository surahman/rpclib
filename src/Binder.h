            /*__________________________________________________#
            #  Life is a journey, and it's the experiences we   #
            #  have along the way that define us. To shy from   #
            #  the difficult path least traveled is a travesty, #
            #  for it's here that we find the greatest bounty.  #
            #                                                   #
            #               ~ Perpetual Form ~                  #
            #                                                   #
            #   For my father, who taught me the true meaning   #
            #                   of courage.                     #
            #___________________________________________________#
            # File Name    :    Binder.h                        #
            # Dependencies :    LIB:                            #
            #                   USR: Binder.cpp                 #
            # Toolchain    :    GNU G++ v6                      #
            # Compilation  :    make binder                     #
            #___________________________________________________#
            #    https://www.linkedin.com/in/saad-ur-rahman/    #
            #  Copyright Saad Ur Rahman, All rights reserved.   #
            #     Usage and source code is subject to the       #
            #           GNU Affero GPL v3.0 license.            #
            #___________________________________________________#
            #                                                   #
            #               Basic Binder Class.                 #
            #__________________________________________________*/



#ifndef _BINDER_H_
#define _BINDER_H_

//- Include(s): Library:
#include <cstddef>

//- Include(s): User:
#include "RPCLib.h"

//- Namespace Import(s):
using std::string;



            /*_________________________________________________ #
            #               Object Definitions                  #
            # _________________________________________________*/



class Binder {
public:
    //-- Constructor(s)/Destructor(s):
    Binder( string portStr = "0",               //- Default constructor.
            int portNum = 0,
            int backLog = 10);
    
    ~Binder();                                  // Destructor.

    //-- Method(s):

private:

    //-- Method(s):
    void Bootstrap(string portNum);             // Setup <Server>s listening socket.
    
    void selector(int fd_Socket);               // Multiplex listen on a single socket.
    
    int getFnSig(int fd_Socket,                 //- Pulls function signature off socket.
                 FnSig& signature);
    
    int getSrvSig(int fd_Socket,                //- Pulls server signature off socket.
                  SrvSig& signature);
    
    int registerFn(int fd_Socket);              // Register a function.
    
    void locationReq(int fd_Socket);            // Location request for <Server> servicing a function.
    
    void terminate();                           // Shutdown <Server>s and then the <Binder>.
    
    void cacheCall(int fd_Socket);              // Scrapes the database for all <Server>s servicing a function.


    //-- Data Member(s):
    int fd_LstnSock;                            // <Binder>s listening socket.
    fd_set fd_Collective;                       // Complete collection of active sockets.
    size_t m_ConnCnt = 0;                       // Number of active connections.
    int m_PortServ;                             // <Binder>s listening port.
    string m_Hostname;                          // <Binder>s hostname.
    const int k_BackLog;                        // Inbound connection backlog size.
    bool m_Active = true;                       // Active status of <Binder>.
    Database m_DBase;                           // Database and scheduler.
};


#endif
