            /*__________________________________________________#
            #  Life is a journey, and it's the experiences we   #
            #  have along the way that define us. To shy from   #
            #  the difficult path least traveled is a travesty, #
            #  for it's here that we find the greatest bounty.  #
            #                                                   #
            #               ~ Perpetual Form ~                  #
            #                                                   #
            #   For my father, who taught me the true meaning   #
            #                   of courage.                     #
            #___________________________________________________#
            # File Name    :    Server.h                        #
            # Dependencies :    LIB:                            #
            #                   USR: Server.cpp                 #
            # Toolchain    :    GNU G++ v6                      #
            # Compilation  :    make rpclib                     #
            #___________________________________________________#
            #    https://www.linkedin.com/in/saad-ur-rahman/    #
            #  Copyright Saad Ur Rahman, All rights reserved.   #
            #     Usage and source code is subject to the       #
            #           GNU Affero GPL v3.0 license.            #
            #___________________________________________________#
            #                                                   #
            #               Basic Server Class.                 #
            #__________________________________________________*/



#ifndef _SERVER_H_
#define _SERVER_H_

//- Include(s): Library:
#include <condition_variable>
#include <mutex>
#include <queue>

//- Include(s): User:
#include "Client.h"
#include "Database.h"
#include "RPCLib.h"

//- Namespace Import(s):
using std::string;



            /*_________________________________________________ #
            #               Object Definitions                  #
            # _________________________________________________*/



class Server {
public:
    //-- Constructor(s)/Destructor(s):
    Server(bool showBanner = false,             //- Default constructor.
           int portNum = 0,
           int backLog = 10);
    
    ~Server();                                  // Destructor.

    //-- Method(s):
    int openListeningSock();                    // Opens <Server>s listening socket.

    int connectToBinder();                      // Opens connection to <Binder>.

    int registerReq(char* name,                 // Registers function with <Binder>.
                    int* argTypes,
                    RPCLib::skeleton f);

    int acceptor();                             // Acceptor loop that accepts new work requests.

private:
    //-- Method(s):
    void Bootstrap(string portNum);             // Setup <Client>s socket.

    void requestHandler();                      // Worker that processes a work request.
    
    void terminateHandler();                    // Terminate all worker threads by placing sentinel <-1>.

    int getExecReq(int fd_Socket,               //- Get the complete <EXECUTE> request off the socket.
                   char* fnName,
                   uint32_t& argCnt,
                   int* &argTypes,
                   std::list<RPCLib::ByteSpec> &byteSize,
                   void** &args);

    void execFn(char* fnName,                   //- <EXECUTE>s the actual request and returns to the <Client>.
                int* argTypes,
                void** args,
                std::list<RPCLib::ByteSpec> &byteSize,
                int fd_Socket);

    //-- Data Member(s):
    const int k_BackLog;                        // Inbound connection backlog size.
    int fd_LstnSock;                            // <Server>s listening socket.
    SrvSig m_ServerAddr;                        // <Server>s address.
    bool m_Active = false;                      // <Server> is accepting job requests.
    SrvSig m_BinderAddr;                        // <Binder>s address.
    Client *m_Binder = nullptr;                 // <Client> to call <Binder>.
    std::thread *trd_Binder = nullptr;          // Thread that checks <Binder>s heart beat.

    std::list<std::thread> m_ThreadPool;        // Thread pool of worker threads.
    std::queue<int> m_JobQueue;                 // Queue of sockets with data waiting on them.
    std::mutex lk_JobQueue;                     // <Job Queue> mutex.
    std::condition_variable cv_JobQueue;        // <Job Queue> condition variable.

    std::unordered_map<FnSig,                   //- Database mapping functions to <skeleton>s.
                       RPCLib::skeleton,
                       FnSigHash>
                       fn_DBase;
    std::mutex lk_FNDBase;                      // Mutex for local database. MIGHT NOT BE NEEDED.

};


#endif
