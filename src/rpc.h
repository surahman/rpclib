            /*__________________________________________________#
            #  Life is a journey, and it's the experiences we   #
            #  have along the way that define us. To shy from   #
            #  the difficult path least traveled is a travesty, #
            #  for it's here that we find the greatest bounty.  #
            #                                                   #
            #               ~ Perpetual Form ~                  #
            #                                                   #
            #   For my father, who taught me the true meaning   #
            #                   of courage.                     #
            #___________________________________________________#
            # File Name    :    rpc.h                           #
            # Dependencies :    LIB:                            #
            #                   USR: rpc.cpp                    #
            # Toolchain    :    GNU G++ v6                      #
            # Compilation  :    make rpclib                     #
            #___________________________________________________#
            #    https://www.linkedin.com/in/saad-ur-rahman/    #
            #  Copyright Saad Ur Rahman, All rights reserved.   #
            #     Usage and source code is subject to the       #
            #           GNU Affero GPL v3.0 license.            #
            #___________________________________________________#
            #                                                   #
            #                Basic RPC Library.                 #
            #__________________________________________________*/



#ifndef _RPC_H_
#define _RPC_H_



//- Include(s): Library:
#include <stdbool.h>



            /*_________________________________________________ #
            #             Global Constant Variables             #
            # _________________________________________________*/

#ifdef __cplusplus
extern "C" {
#endif

#define ARG_CHAR    1
#define ARG_SHORT   2
#define ARG_INT     3
#define ARG_LONG    4
#define ARG_DOUBLE  5
#define ARG_FLOAT   6

#define ARG_INPUT   31
#define ARG_OUTPUT  30



            /*_________________________________________________ #
            #                   Method Stubs                    #
            # _________________________________________________*/

typedef int (*skeleton)(int *,
                        void **);


#define RPCINIT(...) rpcInit( (false, ##__VA_ARGS__) )
extern int rpcInit(bool showBanner);

extern int rpcCall(char* name,
                   int* argTypes,
                   void** args);

extern int rpcCacheCall(char* name,
                        int* argTypes,
                        void** args);

extern int rpcRegister(char* name,
                       int* argTypes,
                       skeleton f);

extern int rpcExecute();

extern int rpcTerminate();

extern int rpcCacheClear();

#ifdef __cplusplus
}
#endif

#endif
