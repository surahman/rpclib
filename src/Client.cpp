            /*__________________________________________________#
            #  Life is a journey, and it's the experiences we   #
            #  have along the way that define us. To shy from   #
            #  the difficult path least traveled is a travesty, #
            #  for it's here that we find the greatest bounty.  #
            #                                                   #
            #               ~ Perpetual Form ~                  #
            #                                                   #
            #   For my father, who taught me the true meaning   #
            #                   of courage.                     #
            #___________________________________________________#
            # File Name    :    Client.cpp                      #
            # Dependencies :    LIB:                            #
            #                   USR: Client.h                   #
            # Toolchain    :    GNU G++ v6                      #
            # Compilation  :    make rpclib                     #
            #___________________________________________________#
            #    https://www.linkedin.com/in/saad-ur-rahman/    #
            #  Copyright Saad Ur Rahman, All rights reserved.   #
            #     Usage and source code is subject to the       #
            #           GNU Affero GPL v3.0 license.            #
            #___________________________________________________#
            #                                                   #
            #               Basic Client Class.                 #
            #__________________________________________________*/



//- Include(s): User:
#include "Client.h"



            /*_________________________________________________ #
            #               Method Definitions                  #
            # _________________________________________________*/



/* Client:
*   Constructor. Setup <TCP/IP> socket and begin communication.
* -Parameters:  string hostAddr = "localhost", string portStr = "0", int portNum = 0
* -Returns:     n/a
* -Throws:      n/a
*/
Client::Client(string hostAddr,
               int portNum) :
               m_Hostname(hostAddr),
               m_PortNum(portNum)
{
    // Variable Declaration(s):
    std::string portStr = std::to_string(portNum);// String <port> representation.

    // If the <portNum> is not provided use <getenv> to get address.
    if (m_PortNum == 0)
    {
        // Get <hostname>.
        char* host = secure_getenv("BINDER_ADDRESS");
        if (host == nullptr)
        {
            throw std::invalid_argument(" Please set the <Server> address Environment Variable.]");
        }
        m_Hostname = host;

        // Get <port>.
        char* port = secure_getenv("BINDER_PORT");
        if (port == nullptr)
        {
            throw std::invalid_argument(" Please set the <Server> port Environment Variable.]");
        }
        portStr = port;

        // Convert to integer <port>.
        m_PortNum = atoi(portStr.c_str());
    }

    // Validate <port> range.
    if (m_PortNum < 1024 || m_PortNum > 65535)
    {
        throw std::invalid_argument(" Invalid port range, must be in range [1024 - 65535].]");
    }

    // Bootstrap the listening socket.
    Bootstrap(portStr);
}



/* ~Client:
*   Destructor: Closes the communication <port>.
* -Parameters:  n/a
* -Returns:     n/a
* -Throws:      n/a
*/
Client::~Client()
{
    // Close <Client>s <TCP/IP> socket.
    close(fd_Socket);
}



/* Bootstrap:
*   Setup <Client>s socket.
* -Parameters:  string portNum
* -Returns:     n/a
* -Throws:      runtime_error
*/
void Client::Bootstrap(string portNum)
{
    // Variable Declaration(s):
    struct addrinfo hints;                      // Hints to help setup socket.
    struct addrinfo *servinfo;                  // Server's listening socket info.
    struct addrinfo *lookup;                    // Address to bind socket too.
    int rv;                                     // Return code for <getaddrinfo>.


    // Setup parameters for socket search.
    memset(&hints, 0, sizeof hints);            // Zero out <hints>.
    hints.ai_family = AF_UNSPEC;                // Either IPv4/IPv6.
    hints.ai_socktype = SOCK_STREAM;            // TCP/IP socket.


    // Reverse DNS and assign to a host.
    if ((rv = getaddrinfo(m_Hostname.c_str(), portNum.c_str(), &hints, &servinfo)) != 0)
    {
        string err = " <getaddrinfo> failed: " + string(gai_strerror(rv)) + "]";
        throw std::runtime_error(err);
    }


    // <Lookup> address and bind to the first available and valid one.
    for (lookup = servinfo; lookup != NULL; lookup = lookup->ai_next)
    {
        // Attempt to bind to correct socket protocol (TCP/IP or UDP).
        if ((fd_Socket = socket(lookup->ai_family, lookup->ai_socktype, lookup->ai_protocol)) == -1)
        {
            continue;
        }

        // Attempt to connect to the server using currently selected socket.
        if (connect(fd_Socket, lookup->ai_addr, lookup->ai_addrlen) == -1)
        {
            close(fd_Socket);
            continue;
        }

        // Connection succeeded, terminate loop.
        break;
    }


    // Housekeeping and check to ensure connection succeeded.
    freeaddrinfo(servinfo);
    if (lookup == NULL)
    {
        throw std::runtime_error(" Failed to connect to server.]");
    }


    // Get the socket port and hostname. Update <fd_LstnSock> and <m_Hostname>.
    std::pair<string, int> addr =  RPCLib::GetAddr(fd_Socket, RPCLib::MsgType::CLIENT);
    m_Hostname = addr.first;
    m_PortNum = addr.second;
    //std::cout << "CLIENT CONNECTED TO: " << m_Hostname << " ON PORT: " << m_PortNum << std::endl;
}



/* sendFnSig:
*   Send a <LOC_REQUEST> to <Binder>.
* -Parameters:  FnSig& signature
* -Returns:     RPCLib::LocQuery
* -Throws:      n/a
*/
int Client::sendFnSig(FnSig& signature) const
{
    //- Variable Declaration(s):
    uint32_t length = 0;                        // <Message> length.

    // Length is the number of <args> here and not the byte size.
    length = htonl(signature.m_paramList.size());

    // Send: <length>
    if (RPCLib::sender(fd_Socket, &length, sizeof(uint32_t)) < 0)
    {
        return -1;
    }

    // Send: <name>
    char fnName[RPCLib::k_StringLength];
    memset(fnName, 0, RPCLib::k_StringBytes);
    signature.m_Name.copy(fnName, signature.m_Name.length());
    if (RPCLib::sender(fd_Socket, fnName, RPCLib::k_StringBytes) < 0)
    {
        return -1;
    }

    // Send: <argTypes>
    for (auto iter = signature.m_paramList.cbegin(); iter != signature.m_paramList.cend(); ++iter)
    {
        uint16_t arg = htons(*iter);
        if (RPCLib::sender(fd_Socket, &arg, RPCLib::k_argTypeBinderBytes) < 0)
        {
            return -1;
        }
    }

    // Success.
    return 0;
}



/* sendExecReq:
*   Send a <EXECUTE> request to <Server>. Returns positive number in even of failure to send/receive.
* -Parameters:  char* name, int* argTypes, void** args, std::list<RPCLib::ByteSpec>& byteSize)
* -Returns:     n/a
* -Throws:      n/a
*/
int Client::sendExecReq(char* name,
                        int* argTypes,
                        void** args,
                        std::list<RPCLib::ByteSpec>& byteSize)
{
    //- Variable Declaration(s):
    uint8_t reqType = -1;                       // Request type.
    uint32_t argCnt = 0;                        // Argument count.
    int retval = 0;                             // Return value of remote execution.


    // Extract argument count and byte size.
    argCnt = RPCLib::argBytes(argTypes, byteSize);


    //- Execute request: <EXECUTE> <name> <argCnt> <argTypes> <args>
    reqType = (uint8_t) RPCLib::MsgType::EXECUTE;

    // Send: <EXECUTE> <name> <argCnt>
    if (RPCLib::sender(fd_Socket, &reqType, RPCLib::k_MsgTypeBytes) <= 0)
    {
        return 1;
    }
    if (RPCLib::sender(fd_Socket, name, RPCLib::k_StringBytes) <= 0)
    {
        return 1;
    }
    uint32_t nbo_argCnt = htonl(argCnt);
    if (RPCLib::sender(fd_Socket, &nbo_argCnt, RPCLib::k_argTypeBytes) <= 0)
    {
        return 1;
    }

    // Send: <argTypes>
    for (size_t i = 0; argTypes[i] != 0; ++i)
    {
        uint32_t data = htonl(argTypes[i]);
        if (RPCLib::sender(fd_Socket, &data, RPCLib::k_argTypeBytes) <= 0)
        {
            return 1;
        }
    }

    // Send: <args>
    auto iter = byteSize.cbegin();
    for (size_t i = 0; i < argCnt; ++i, ++iter)
    {
        // Switch to network byte order.
        if (iter->m_Convert == true)
        {
            RPCLib::byteOrderSwap(args[i], iter->m_Size);
        }

        // Send argument.
        if (RPCLib::sender(fd_Socket, args[i], iter->m_Size) <= 0)
        {
            return 2;
        }
    }

    // Success.
    return retval;
}



/* resultExecReq:
*   Receive result of <EXECUTE> request from <Server>. Return positive number greater than 10 on failure to xfer.
* -Parameters:  void** &args, std::list<RPCLib::ByteSpec>& byteSize
* -Returns:     n/a
* -Throws:      n/a
*/
int Client::resultExecReq(void** &args,
                          std::list<RPCLib::ByteSpec>& byteSize)
{
    //- Variable Declaration(s):
    uint8_t reqType = -1;                       // Request type.
    int32_t code = 0;                           // Failure code.

    // Get <reqType>.
    if (RPCLib::receiver(fd_Socket, &reqType, RPCLib::k_MsgTypeBytes) < 0)
    {
        return 11;
    }

    // <EXECUTE_SUCCESS>:
    if (reqType == (uint8_t) RPCLib::MsgType::EXECUTE_SUCCESS)
    {
        // Pull <args> off socket.
        auto iter = byteSize.cbegin();
        size_t argCnt = byteSize.size();
        for (size_t i = 0; i < argCnt; ++i, ++iter)
        {
            // Get bytes off socket.
            if (RPCLib::receiver(fd_Socket, args[i], iter->m_Size) <= 0)
            {
                return 12;
            }

            // Convert to host byte order.
            if (iter->m_Convert == true)
            {
                RPCLib::byteOrderSwap(args[i], iter->m_Size);
            }
        }

    }
    // <EXECUTE_FAILURE>:
    else
    {
        // Get failure code off the socket.
        if (RPCLib::receiver(fd_Socket, &code, RPCLib::k_ReasonCodeBytes) <= 0)
        {
            return 13;
        }
        code = ntohl(code);
    }

    // Return the status <code>.
    return code;
}


/* locationReq:
*   Send a <LOC_REQUEST> to <Binder>.
* -Parameters:  FnSig& signature
* -Returns:     RPCLib::LocQuery
* -Throws:      n/a
*/
RPCLib::LocQuery Client::locationReq(FnSig& signature)
{
    //- Variable Declaration(s):
    uint8_t reqType = -1;                       // Request type.
    uint16_t portNum = 0;                       // <Server>s <port> number.
    string hostname;                            // <Server>s <hostname>.


    //- Location request: <LOC_REQUEST> <length> <name> <argTypes>
    // Send <LOC_REQUEST>.
    reqType = (uint8_t)RPCLib::MsgType::LOC_REQUEST;
    if (RPCLib::sender(fd_Socket, &reqType, RPCLib::k_MsgTypeBytes) <= 0)
    {
        return RPCLib::LocQuery(-1);
    }

    // Send function signature.
    if (sendFnSig(signature) < 0)
    {
        return RPCLib::LocQuery(-1);
    }


    //- Get response from <Binder>:
    // Get <reqType>.
    if (RPCLib::receiver(fd_Socket, &reqType, RPCLib::k_MsgTypeBytes) <= 0)
    {
        return RPCLib::LocQuery(-1);
    }

    // <LOC_SUCCESS>:
    if ((RPCLib::MsgType) reqType == RPCLib::MsgType::LOC_SUCCESS)
    {
        // Get <hostname>.
        char buffer[RPCLib::k_StringLength];
        memset(buffer, 0, RPCLib::k_StringBytes);
        if (RPCLib::receiver(fd_Socket, buffer, RPCLib::k_StringBytes) <= 0)
        {
            return RPCLib::LocQuery(-2);
        }

        // Get <port> number.
        if (RPCLib::receiver(fd_Socket, &portNum, RPCLib::k_PortBytes) <= 0)
        {
            return RPCLib::LocQuery(-2);
        }
        portNum = ntohs(portNum);

        // Convert to string and clear memory.
        hostname = buffer;

        // Pack return values.
        return RPCLib::LocQuery(0, RPCLib::MsgType::LOC_SUCCESS, hostname, portNum);
    }
    // <LOC_FAILURE>:
    else
    {
        // Pull error <code> off socket.
        int16_t code = 0;
        if (RPCLib::receiver(fd_Socket, &code, RPCLib::k_MsgTypeBytes) <= 0)
        {
            return RPCLib::LocQuery(-2);
        }

        // Pack return values, and error code.
        return RPCLib::LocQuery(code, RPCLib::MsgType::LOC_FAILURE, hostname, portNum);
    }
}



/* registrationReq:
*   Send <REGISTER> request to <Binder>.
* -Parameters:  SrvSig& srvAddr, FnSig& signature
* -Returns:     int
* -Throws:      n/a
*/
int Client::registrationReq(SrvSig& srvAddr,
                            FnSig& signature)
{
    //- Variable Declaration(s):
    uint8_t reqType = -1;                       // Request type.
    char hostname[RPCLib::k_StringLength];      // <Server>s <hostname>.
    int32_t code = 0;                           // Error code.


    //- Location request: <REGISTER> <hostname> <port> <length> <name> <argTypes>
    // Send <REGISTER>.
    reqType = (uint8_t) RPCLib::MsgType::REGISTER;
    if (RPCLib::sender(fd_Socket, &reqType, RPCLib::k_MsgTypeBytes) <= 0)
    {
        return -1;
    }

    // Send <hostname>.
    memset(hostname, 0, RPCLib::k_StringBytes);
    srvAddr.m_Hostname.copy(hostname, srvAddr.m_Hostname.length());
    if (RPCLib::sender(fd_Socket, &hostname, RPCLib::k_StringBytes) <= 0)
    {
        return -1;
    }

    // Send <port>.
    uint16_t port = htons(srvAddr.m_Port);
    if (RPCLib::sender(fd_Socket, &port, RPCLib::k_PortBytes) <= 0)
    {
        return -1;
    }

    // Send function signature.
    if (sendFnSig(signature) < 0)
    {
        return -1;
    }

    // Get registration response back from <Binder>: <MsgType> <Code>
    if (RPCLib::receiver(fd_Socket, &reqType, RPCLib::k_MsgTypeBytes) <= 0)
    {
        return -2;
    }
    if (RPCLib::receiver(fd_Socket, &code, RPCLib::k_ReasonCodeBytes) <= 0)
    {
        return -2;
    }
    code = ntohl(code);

    // Success.
    return code;
}



/* executeReq:
*   Send a <EXECUTE> request to <Server>. Send back positive codes for failures in send/receive.
* -Parameters:  char* name, int* argTypes, void** args
* -Returns:     int
* -Throws:      n/a
*/
int Client::executeReq(char* name,
                       int* argTypes,
                       void** args)
{
    //- Variable Declaration(s):
    int32_t code = -1;                          // Error code.
    std::list<RPCLib::ByteSpec> byteSize;       // Byte size of <args>.

    // Send the <EXECUTE> request to the <Server>.
    if (sendExecReq(name, argTypes, args, byteSize) < 0)
    {
        return 1;
    }


    //- Get response from <Server>: Positive codes are failures to receive data.
    code = resultExecReq(args, byteSize);
    if (code > 10)
    {
        return 2;
    }

    // Success.
    return code;
}



/* terminateReq:
*   Send <terminate> request to <Binder>.
* -Parameters:  n/a
* -Returns:     int
* -Throws:      n/a
*/
int Client::terminateReq()
{
    //- Variable Declaration(s):
    uint8_t msg = -1;                           // <Terminate> message.

    // Send <terminate> message.
    msg = (uint8_t) RPCLib::MsgType::TERMINATE;
    if (RPCLib::sender(fd_Socket, &msg, RPCLib::k_MsgTypeBytes) < 0)
    {
        return -1;
    }

    // Success.
    return 0;
}



/* binderHeartBeat:
*   Checks the pulse of <Binder>, blocks until it flat lines, returning <0>.
* -Parameters:  n/a
* -Returns:     int
* -Throws:      n/a
*/
int Client::binderHeartBeat()
{
    //- Variable Declaration(s):
    uint8_t msg = -1;                           // <Terminate> message.

    // Non-busy wait for heart-beat, terminates on closed connection.
    while (true)
    {
        if (RPCLib::receiver(fd_Socket, &msg, RPCLib::k_MsgTypeBytes) == 0)
        {
            return 0;
        }
    }
}



/* cacheCall:
*   Request list of servicing <Server>s from <Binder>.
* -Parameters:  FnSig& signature, std::list<SrvSig>& cache
* -Returns:     int
* -Throws:      n/a
*/
int Client::cacheCall(FnSig& signature,
                      std::list<SrvSig>& cache)
{
    //- Variable Declaration(s):
    uint8_t reqType = -1;                       // Request type.


    //- Cache call: <CACHE_CALL> <length> <name> <argTypes>
    // Send <CACHE_CALL>.
    reqType = (uint8_t)RPCLib::MsgType::CACHE_CALL;
    if (RPCLib::sender(fd_Socket, &reqType, RPCLib::k_MsgTypeBytes) <= 0)
    {
        return -1;
    }

    // Send function signature.
    if (sendFnSig(signature) < 0)
    {
        return -1;
    }


    //- Receive cache call: <CACHE_SUCCESS> <length> [<hostname> <port>]+
    // Get <reqType>.
    if (RPCLib::receiver(fd_Socket, &reqType, RPCLib::k_MsgTypeBytes) <= 0)
    {
        return -2;
    }

    // Search hit.
    if ((RPCLib::MsgType) reqType == RPCLib::MsgType::CACHE_SUCCESS)
    {
        // Get <Server> count.
        uint32_t count = 0;
        if (RPCLib::receiver(fd_Socket, &count, RPCLib::k_LengthBytes) <= 0)
        {
            return -2;
        }
        count = ntohl(count);

        // Loop over input.
        for (size_t i = 0; i < count; ++i)
        {
            // Get <hostname>.
            char buffer[RPCLib::k_StringLength];
            memset(buffer, 0, RPCLib::k_StringBytes);
            if (RPCLib::receiver(fd_Socket, buffer, RPCLib::k_StringBytes) <= 0)
            {
                return -2;
            }

            // Get <port> number.
            uint16_t portNum = 0;
            if (RPCLib::receiver(fd_Socket, &portNum, RPCLib::k_PortBytes) <= 0)
            {
                return -2;
            }
            portNum = ntohs(portNum);

            // Insert the <SrvSig> into the <cache>.
            cache.emplace_back(SrvSig(buffer, portNum));
        }
    }
    // Search miss.
    else
    {
        return -3;
    }


    // Success.
    return 0;
}
