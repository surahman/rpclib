            /*__________________________________________________#
            #  Life is a journey, and it's the experiences we   #
            #  have along the way that define us. To shy from   #
            #  the difficult path least traveled is a travesty, #
            #  for it's here that we find the greatest bounty.  #
            #                                                   #
            #               ~ Perpetual Form ~                  #
            #                                                   #
            #   For my father, who taught me the true meaning   #
            #                   of courage.                     #
            #___________________________________________________#
            # File Name    :    RPCLib.cpp                      #
            # Dependencies :    LIB:                            #
            #                   USR: RPCLib.h                   #
            # Toolchain    :    GNU G++ v6                      #
            # Compilation  :    make rpclib                     #
            #___________________________________________________#
            #    https://www.linkedin.com/in/saad-ur-rahman/    #
            #  Copyright Saad Ur Rahman, All rights reserved.   #
            #     Usage and source code is subject to the       #
            #           GNU Affero GPL v3.0 license.            #
            #___________________________________________________#
            #                                                   #
            #                Basic RPC Library.                 #
            #__________________________________________________*/



//- Include(s): Library:
#include <cassert>
#include <cstring>

//- Include(s): User:
#include "RPCLib.h"



            /*_________________________________________________ #
            #               Method Definitions                  #
            # _________________________________________________*/



/* GetAddr:
*   Extracts hostname and port number from a socket file descriptor.
* -Parameters:  int fd_Socket, RPCLib::MsgType type
* -Returns:     string hostname, int port
* -Throws:      runtime_error
*/
std::pair<string, int> RPCLib::GetAddr(int fd_Socket,
                                       RPCLib::MsgType type)
{
    // Variable Declaration(s):
    struct sockaddr_in sockAddr;                // Address to structure for listening socket.
    socklen_t sockLen = sizeof(sockAddr);       // Size (bytes) of the listening socket.
    char hostname[32];                          // Canonical hostname associated with socket.
    int port = -1;                              // Port number associated with socket.


    // Extract port number form <fd_Socket>.
    if (getsockname(fd_Socket, (struct sockaddr *) &sockAddr, &sockLen) == -1)
    {
        throw std::runtime_error(" Failed to recover listening port number.]");
    }
    port = ntohs(sockAddr.sin_port);


    // [CLIENT]:
    if (type == RPCLib::MsgType::CLIENT)
    {
        // Extract <hostname> from <fd_Socket>.
        socklen_t addrSize = sizeof sockAddr;
        if (getpeername(fd_Socket, (struct sockaddr *) &sockAddr, &addrSize) == -1)
        {
            throw std::runtime_error(" Failed to recover clients hostname.]");
        }

        strcpy(hostname, inet_ntoa(sockAddr.sin_addr));
    }
    // [SERVER]:
    else
    {
        // Extract <hostname> from <fd_Socket>.
        if (gethostname(hostname, sizeof hostname) == -1)
        {
            throw std::runtime_error(" Failed to recover servers hostname.]");
        }
    }

    return std::make_pair(hostname, port);
}



/* sender:
*   Ensures that the complete <msg> is transmitted over <socket>.
* -Parameters:  int socket, const void* payload, size_t remaining
* -Returns:     int
* -Throws:      n/a
*/
int RPCLib::sender(int socket,
                   const void* payload,
                   size_t remaining)
{
    // Variable Declaration(s):
    int sent = 0;                               // Total number of bytes transmitted.
    uint8_t* position = (uint8_t*) payload;     // Pointer into <buffer> at 1-byte increments.

    // Enclosed in try-catch to capture all errors thrown by <send>.
    try
    {
        // Loop till <send> is complete.
        while (remaining > 0)
        {
            // Send bytes to <TCP/IP> layer.
            if ((sent = send(socket, position, remaining, 0)) <= 0)
            {
                throw std::runtime_error("ERROR: sending data on socket.");
            }

            position += sent;
            remaining -= sent;

            //std::cout << "SENT: " << sent << std::endl;
        }
    }
    catch (...)
    {
    }

    // On success return the bytes sent or negative failure code.
    return sent;
}



/* receiver:
*   Receive a full message of <expectedSize> into <buffer>.
* -Parameters:  int socket, void* buffer, size_t expectedSize
* -Returns:     int
* -Throws:      n/a
*/
int RPCLib::receiver(int socket,
                     void* buffer,
                     size_t expectedSize)
{
    // Variable Declaration(s):
    uint32_t received = 0;                      // Bytes received per loop.
    uint32_t totalBytes = 0;                    // Total number of bytes received.
    uint32_t recvBuffer = 1024;                 // Receiver buffer size.
    uint8_t *position = (uint8_t*)buffer;       // Setup pointer that increments at 1-byte.

    // Sanitize the <buffer>.
    memset(buffer, 0, expectedSize);

    // Enclosed in try-catch to capture all errors thrown by <recv>.
    try
    {
        // Loop until all expected bytes have been received.
        while (expectedSize > 0)
        {
            //std::cerr << "RECEIVED: " << received << "\tEXPECTED SIZE: " << expectedSize << std::endl;

            // Set Receiver Buffer size.
            if (expectedSize < recvBuffer)
            {
                recvBuffer = expectedSize;
            }

            // Receive bytes on the socket.
            if ((received = recv(socket, position, recvBuffer, 0)) <= 0)
            {
                totalBytes = received;
                throw std::runtime_error("ERROR: receiving data on socket.");
            }

            // Decrement the expected file size.
            expectedSize -= received;
            position += received;
            totalBytes += received;
        }
    }
    catch (...)
    {
    }

    // On success return the bytes sent or negative failure code.
    return totalBytes;
}



/* FnSigBuilder:
*   Packs the <FnSig> for location request to the <Binder>.
* -Parameters:  const char* fnName, int* argTypes, FnSig& retval
* -Returns:     n/a
* -Throws:      n/a
*/
void RPCLib::FnSigBuilder(const char* fnName,
                          int* argTypes,
                          FnSig& retval)
{
    // Set the <function> name.
    retval.m_Name = fnName;

    // Loop over <argType> adding them to the <Record>.
    for (size_t i = 0; argTypes[i] != 0; ++i)
    {
        // Separate lower and higher order 16-bits.
        uint32_t arg = argTypes[i];
        uint16_t arraySet = arg;
        uint16_t entry = arg >> 16;

        // Set the array bit (13th bit) in <entry> and insert.
        if (arraySet)
        {
            entry |= 1UL << 13;
        }
        retval.m_paramList.push_back(entry);
    }
}



/* argBytes:
*   Get <args> size from the <argTypes> array. <first = args count> <second = args byte size>.
* -Parameters:  int* argTypes, std::list<uint32_t> &byteSz
* -Returns:     uint32_t
* -Throws:      n/a
*/
uint32_t RPCLib::argBytes(int* argTypes,
                          std::list<RPCLib::ByteSpec> &byteSz)
{
    // Variable Declaration(s):
    uint32_t argCnt = 0;                        // Number of arguments for function.
    uint32_t argBytes = 0;                      // Number of bytes in argument.
    uint16_t count = 0;                         // Total number of items in params <args>.
    bool convertOrder = false;                  // Check for byte order conversion.


    // Iterate over <argTypes> and populate the parameter specs.
    for (; argTypes[argCnt] != 0; ++argCnt)
    {
        // Extract array size.
        uint16_t arraySz = argTypes[argCnt];

        // Extract data type information.
        uint8_t type = argTypes[argCnt] >> 16;


        // Calculate total space in running <totalBytes>.
        arraySz == 0 ? count = 1 : count = arraySz;
        switch ((RPCLib::DataType) type)
        {
        case RPCLib::DataType::ARG_CHAR:
            argBytes = sizeof(char) * count;
            convertOrder = false;
            break;
        case RPCLib::DataType::ARG_SHORT:
            argBytes = sizeof(short) * count;
            convertOrder = true;
            break;
        case RPCLib::DataType::ARG_INT:
            argBytes = sizeof(int) * count;
            convertOrder = true;
            break;
        case RPCLib::DataType::ARG_LONG:
            argBytes = sizeof(long) * count;
            convertOrder = true;
            break;
        case RPCLib::DataType::ARG_DOUBLE:
            argBytes = sizeof(double) * count;
            convertOrder = true;
            break;
        case RPCLib::DataType::ARG_FLOAT:
            argBytes = sizeof(float) * count;
            convertOrder = true;
            break;
        default:
            break;
        }

        // Store each <args> byte size.
        byteSz.push_back(RPCLib::ByteSpec(argBytes, convertOrder));
    }

    // Total size of <args> payload.
    return argCnt;
}



/* byteOrderSwap:
*   Converts <args> to network byte-order.
* -Parameters:  void *buffer, size_t length
* -Returns:     n/a
* -Throws:      n/a
*/
void RPCLib::byteOrderSwap(void *buffer,
                           const size_t length)
{
    // Variable Declaration(s):
    int8_t* begin = (int8_t *) buffer;          // Beginning of byte buffer.
    int8_t* end = begin + length - 1;           // End of byte buffer.
    int8_t temp;                                // Temporary buffer.

    // Loop over the memory segment swapping bytes.
    while (begin < end)
    {
        temp = *begin;
        *begin++ = *end;
        *end-- = temp;
    }
}



/* banner:
*   Displays the MOTD banner.
* -Parameters:  bannerType process = bannerType::BANNER_NONE
* -Returns:     n/a
* -Throws:      n/a
*/
void RPCLib::banner(RPCLib::BannerType process)
{

    // Don't display banner.
    if (process == RPCLib::BannerType::BANNER_NONE)     return;


    // Static banner.
    std::cout << "\n\n\n"
        << "                8888888b.  8888888b.   .d8888b.  888      d8b 888\n"
        << "                888   Y88b 888   Y88b d88P  Y88b 888      Y8P 888\n"
        << "                888    888 888    888 888    888 888          888\n"
        << "                888   d88P 888   d88P 888        888      888 88888b.\n"
        << "                8888888P\"  8888888P\"  888        888      888 888 \"88b\n"
        << "                888 T88b   888        888    888 888      888 888  888\n"
        << "                888  T88b  888        Y88b  d88P 888      888 888 d88P\n"
        << "                888   T88b 888         \"Y8888P\"  88888888 888 88888P\"\n"
        << "\n\n"
        << "                                  Saad Ur Rahman\n"
        << "                     https://www.linkedin.com/in/saad-ur-rahman/ \n\n\n";



    // Dynamic component.
    std::string label = "";
    switch ((RPCLib::BannerType) process)
    {
    case RPCLib::BannerType::BANNER_BINDER:
        label = "   BINDER   ";
        break;
    case RPCLib::BannerType::BANNER_SERVER:
        label = "   SERVER   ";
        break;
    case RPCLib::BannerType::BANNER_CLIENT:
        label = "   CLIENT   ";
        break;
    default:
        break;
    }


    std::cout << "                                  [" + label + "]\n\n\n";

}



