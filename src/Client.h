            /*__________________________________________________#
            #  Life is a journey, and it's the experiences we   #
            #  have along the way that define us. To shy from   #
            #  the difficult path least traveled is a travesty, #
            #  for it's here that we find the greatest bounty.  #
            #                                                   #
            #               ~ Perpetual Form ~                  #
            #                                                   #
            #   For my father, who taught me the true meaning   #
            #                   of courage.                     #
            #___________________________________________________#
            # File Name    :    Client.h                        #
            # Dependencies :    LIB:                            #
            #                   USR: Client.cpp                 #
            # Toolchain    :    GNU G++ v6                      #
            # Compilation  :    make rpclib                     #
            #___________________________________________________#
            #    https://www.linkedin.com/in/saad-ur-rahman/    #
            #  Copyright Saad Ur Rahman, All rights reserved.   #
            #     Usage and source code is subject to the       #
            #           GNU Affero GPL v3.0 license.            #
            #___________________________________________________#
            #                                                   #
            #               Basic Client Class.                 #
            #__________________________________________________*/



#ifndef _CLIENT_H_
#define _CLIENT_H_

//- Include(s): Library:

//- Include(s): User:
#include "Database.h"
#include "RPCLib.h"

//- Namespace Import(s):
using std::string;



            /*_________________________________________________ #
            #               Object Definitions                  #
            # _________________________________________________*/



class Client {
public:
    //-- Constructor(s)/Destructor(s):
    Client(string hostAddr = "localhost",       //- Default constructor.
           int portNum = 0);
    
    ~Client();                                  // Destructor.

    //-- Method(s):
                                                //- Send a <LOC_REQUEST> to <Binder>.
    RPCLib::LocQuery locationReq(FnSig& signature);

    int registrationReq(SrvSig& srvAddr,        //- Send <REGISTER> request to <Binder>.
                        FnSig& signature);
    
    int executeReq(char* name,                  //- Handle an <EXECUTE> request to <Server>.
                   int* argTypes,
                   void** args);
    
    int terminateReq();                         // Send <terminate> request to <Binder>.
    
    int binderHeartBeat();                      // Checks the online status of the <Binder>.
    
    int cacheCall(FnSig& signature,             //- Request list of servicing <Server>s from <Binder>.
                  std::list<SrvSig>& cache);


private:
    //-- Method(s):
    void Bootstrap(string portNum);             // Setup <Client>s socket.
    
    int sendFnSig(FnSig& signature) const;      // Send the function signature to <Binder>.
    
    int sendExecReq(char* name,                 //- Send <EXECUTE> message to <Server>.
                    int* argTypes,
                    void** args,
                    std::list<RPCLib::ByteSpec>& byteSize);
    
    int resultExecReq(void** &args,             //- Gets result of <EXECUTE> request from <Server>.
                      std::list<RPCLib::ByteSpec>& byteSize);

    //-- Data Member(s):
    string m_Hostname;                          // Servers hostname.
    int fd_Socket;                              // Clients socket file descriptor.
    int m_PortNum;                              // Client socket port number.
    const uint32_t k_BufSize = 1024;            // Message buffer size.
};


#endif
