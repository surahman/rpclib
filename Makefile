#               '--.  '-.      .-'
#                 _,     "     `>
#                  _.-'""""""'--.._
#              .-'` _   .-.   .-.  `._
#           ,'/   (_,'   `-'   `-'    `.;
#       _.-'                             '-._
#      (  __.-     ,.   /;    _`._,     __,,_)
#       `-    `._.'  `-'  \_.'     `._.'  /
#         \                              /
#          `----------------------------'
#                SHAKE N' BAKE BABY!
#
#					Saad Ur Rahman
#



#############################################################
#                   COMPILER SETUP FLAGS.                   #
#############################################################
OPT:=-O2 -lpthread                                          # Optional compiler flags.
CXX = g++                                                   # Compiler.
CXXFLAGS = -Wall ${OPT} -MMD -std=c++11                     # Compiler flags.


#############################################################
#              DIRECTORY AND FILE NAME SETUP.               #
#############################################################
MKDIR_P = mkdir -p                                          # Create directory command.

SRC_DIR = src/
BIN_DIR = bin/
LIB_DIR = lib/
TEST_DIR = extern_test/

BINDER_FILES = MainBinder Binder RPCLib Database
LIBRARY_FILES = rpc RPCLib Database Client Server
CLIENT_FILES = client1
SERVER_FILES = server server_function_skels server_functions


#############################################################
#             BUILD, COMPILE AND LINK ALIASES.              #
#############################################################
MAKEFILE_NAME = ${firstword ${MAKEFILE_LIST}}

SOURCE_BINDER = $(patsubst %, ${SRC_DIR}%.cpp, ${BINDER_FILES})
OBJECTS_BINDER = $(patsubst %, ${SRC_DIR}%.o, ${BINDER_FILES})
EXEC_BINDER = binder

SOURCE_LIBRARY = $(patsubst %, ${SRC_DIR}%.cpp, ${LIBRARY_FILES})
OBJECTS_LIBRARY = $(patsubst %, ${SRC_DIR}%.o, ${LIBRARY_FILES})
EXEC_LIBRARY = librpc.a

SOURCE_CLIENT = $(patsubst %, ${TEST_DIR}%.c, ${CLIENT_FILES})
OBJECTS_CLIENT = $(patsubst %, ${TEST_DIR}%.o, ${CLIENT_FILES})
EXEC_CLIENT = client

SOURCE_SERVER = $(patsubst %, ${TEST_DIR}%.c, ${SERVER_FILES})
OBJECTS_SERVER = $(patsubst %, ${TEST_DIR}%.o, ${SERVER_FILES})
EXEC_SERVER = server

OBJECTS = ${OBJECTS_BINDER} ${OBJECTS_LIBRARY} ${OBJECTS_SERVER} ${OBJECTS_CLIENT}

EXECS = ${EXEC_BINDER} ${EXEC_LIBRARY} ${EXEC_CLIENT} ${EXEC_SERVER}

DEPENDS = ${OBJECTS:.o=.d}


#############################################################
#              BUILD, COMPILE AND LINK RECIPES.             #
#############################################################
.PHONY : all clean                                          # Command Recipes.

all : rpclib binder client server                           # Build all targets.

binder : ${OBJECTS_BINDER}                                  # Build with call to <Make binder>.
	${MKDIR_P} ${BIN_DIR}
	${CXX} ${CXXFLAGS} ${SOURCE_BINDER} -o ${BIN_DIR}${EXEC_BINDER}

rpclib : ${OBJECTS_LIBRARY}                                 # Build with call to <Make library>.
	${MKDIR_P} ${LIB_DIR}
	${CXX} ${CXXFLAGS} ${OPT} -c ${SOURCE_LIBRARY}
	ar rcs ${LIB_DIR}${EXEC_LIBRARY} ${OBJECTS_LIBRARY}

client : ${OBJECTS_CLIENT}                                  # Build with call to <Make client>.
	${MKDIR_P} ${BIN_DIR}
	${CXX} ${CXXFLAGS} -c ${SOURCE_CLIENT}
	$(CXX) ${CXXFLAGS} -L${LIB_DIR} ${OBJECTS_CLIENT} -lrpc -lpthread -o ${BIN_DIR}${EXEC_CLIENT}

server : ${OBJECTS_SERVER}                                  # Build with call to <Make server>.
	${MKDIR_P} ${BIN_DIR}
	${CXX} ${CXXFLAGS} -c ${SOURCE_SERVER}
	$(CXX) ${CXXFLAGS} -L${LIB_DIR} ${OBJECTS_SERVER} -lrpc -lpthread -o ${BIN_DIR}${EXEC_SERVER}

production : binder rpclib                                  # Build with call to <Make production>.
	find . ./ -type f \( -iname \*.d -o -iname \*.o \) -delete

demo :                                                      # Build with call to <Make demo>.
	$(MAKE) rpclib binder
	$(MAKE) client server
	find . ./ -type f \( -iname \*.d -o -iname \*.o \) -delete


#############################################################
#             MISC ALIASES, COMMANDS AND FLAGS.             #
#############################################################
${OBJECTS} : ${MAKEFILE_NAME}                               # OPTIONAL : changes to this file => recompile.

-include ${DEPENDS}                                         # Include *.d files containing program dependences.

clean :                                                     # Remove files that can be regenerated.
	find . ./ -type f \( -iname \*.d -o -iname \*.o \) -delete
	rm -rf ${LIB_DIR} ${BIN_DIR}
