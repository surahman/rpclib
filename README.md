```bash
                            8888888b.  8888888b.   .d8888b.  888      d8b 888      
                            888   Y88b 888   Y88b d88P  Y88b 888      Y8P 888      
                            888    888 888    888 888    888 888          888      
                            888   d88P 888   d88P 888        888      888 88888b.  
                            8888888P"  8888888P"  888        888      888 888 "88b 
                            888 T88b   888        888    888 888      888 888  888 
                            888  T88b  888        Y88b  d88P 888      888 888 d88P 
                            888   T88b 888         "Y8888P"  88888888 888 88888P"  
```

<p align="center" width="100%">
    <img src="./img/256px.png">
</p>


[ License ] |
:---------: |

This project is made available under the `GNU Affero General Public License v3.0` for the sole purpose of demonstrating
my programming and software development capabilities. Please adhere to the permissions, conditions and limitations
which are outlined in the `GNU Affero General Public License v3.0`.

The license file is included as `LICENSE` in the root directory of this project and can also be found online 
[here](https://opensource.org/licenses/AGPL-3.0 "GNU Affero General Public License v3.0")

<br>
<br>

[ System Specs ] |
:---------: |

This was the system configuration of the local development and test environments. Whilst it is recommended to have a
similar setup, newer versions of the tool sets should not break the build. An up to date `build-essential` package from
the Linux or Debian APT is the ideal toolchain.

|Software| Version
|:------------: | :-------------:|
GCC Compiler | Build Ubuntu 7.5.0-3ubuntu1~18.04
Thread Model | posix
GNU Make | Build 4.1
C++ | Std C++11
Windows 10 Pro (Development) | 19041.572 x64
Ubuntu (Build/Test/Run) | Ubuntu 18.04.5 LTS
Ubuntu Kernel (Build/Test/Run) | 4.19.128-microsoft-standard

<br>
<br>

|[ Build ] |
|:---------: |

[![pipeline status](https://gitlab.com/surahman/rpclib/badges/main/pipeline.svg)](https://gitlab.com/surahman/rpclib/-/commits/main)

- [x] `Binder` & `RPCLib`: Run `make all` to build the binder and linkable library. This will leave behind the
build artifacts.
- [x] `RPCLib`: Run `make rpclib` to build the linkable library. This will leave behind the build artifacts.
- [x] Testing `Client`: Run `make client` to build the externally developed testing client. This will leave behind
the build artifacts.
- [x] Testing `Server`: Run `make server` to build the externally developed testing server. This will leave behind
the build artifacts.
- [x] `Production`: Run `make production` to build both the `binder` and `rpclib`, and then remove all build artifacts.
- [x] `Demo`: Run `make demo` to build the `binder`, `rpclib`,`client`, and the `server`. All build artifacts are
removed upon completion of the build.

Use the `-j <number of threads>` flag with the `make` command to speed up builds. e.g.: `make -j 6 demo`.

<br>

_The flurry of compiler warnings you will get are not from my code but that of the `extern_test`s. Sorry :confounded:._

<br/>

#### _*Linking the library for custom `Server`s and `Client`s:*_

- [x] `Custom`: Please execute the following, and note that the `pthread` library must be linked.

```bash
g++ <your compiler flags> -c <your Server or Client c/cpp source files>
g++ <your compiler flags> -L. <your Server or Client object files> -lrpc -lpthread -o <name of your Server or Client>
```

<br>
<br>

|[ Usage ] |
|:---------: |

To startup the `RPCLib` relay network you will need to follow these steps:

1. Start the `Binder` to support the relay and connection of `Client`s to `Server`s. Please take note of the following
lines which will be printed to `stdout` in the console:

```bash
BINDER_ADDRESS <binders address>
BINDER_PORT <binders port>
```

The `Binder` takes the optional parameters for the listening port and the maximum connection backlog:

```bash
./binder <port> <backlog count>
```


2. The `Client` and `Server` rely on environment variables which must be set so that they can locate the `Binder`.
Please run the following commands in each `bash` shell window from which you launch a `Client` or `Server`:

```bash
export BINDER_ADDRESS=<binders address>
export BINDER_PORT=<binders port>
```

The `Binder` will output the bash environment variable export commands you need to enter in other shell windows on boot.
If you are running the `RPCLib` components on a single machine, please use the loopback hostname `localhost` to avoid
issues connecting to the `Binder`.

<br>

I have successfully conducted tests using 9 `EC2 t2.micro` instances in an AWS VPC and another two hosts
(`Client` & `Sever`) on my local network connecting to the `Binder` `EC2` instance. Traffic between the `EC2` instances
in the AWS VPC was a mixture of localized as well as some which was routed out through the VPC's `NAT Gateway`. This was
achieved using the instance's public and private IP addresses and canonical hostnames.

<br>

The number of `Client`s and `Server`s you can launch are limited to your system's hardware resources and available
ports.

<br>
<br>

|[ Design & Architecture ] |
|:---------: |

<br>

This project is the _pièce de résistance_ in my portfolio. It required a total of 16 days to complete, with 3 days to
design and 13 days to develop, test, and debug. The project began with a suggested protocol, which I completely
redesigned, and a set of external tests (located in the `extern_test` directory). My only guidance on this endeavour
was the Linux Manpages and the `C++11` standard's documentation.

<br>

The complete design documentation can be found **[here](./Design_Document.pdf "design document")** in `PDF` format. It
is a highly recommended read as it outlines the _systems design_, _specification_, and _operation_ of the library and
its integral components.

<br>
<br>

|[ Customization ] |
|:---------: |

<br>

It is assumed that the audience of this project has a background in Computer & Data Systems with experience in software
development in `C/C++`. It is **imperative** that developers **read** the aforementioned
**[design documentation](./Design_Document.pdf "design document")** before venturing forth.

The code snippets which follow are self explanatory and the intended audience should be able to abstract and adapt usage
to their needs. Please refer to the code contained in the `extern_test` directory for additional examples.

<br>

The library abstracts away all the details of the lower level system calls and internal workings. This leaves developers
who leverage the library with a simple, clean, functional, high performance, and low latency interface.

<br>

_For building and linkage instructions please see the prior section titled **Build**._

<br>

#### _Argument Types_

`argTypes` is an array of data representing data type information and is used by the `Binder` to setup the function
signatures database. This database is referenced when connecting a `Client` to a `Server`. Since the number of
arguments is variable the array must be `null` terminated by setting the last element to a value of `0`.

Each argument type must be set in the `argTypes` array by bit-twiddling the argument type and the data bytes into the
correct positions. These specific bytes assignments are defined in the `rpc.h` file. Please see examples below for
details on setup and usage.

<br>

#### _Argument Data_

The actual argument data to be passed to the executing `Server` must be stored in an array of `void` pointers on the
heap. Passing the addresses of variables allocated on the stack will allow for clean syntax when manipulating the
results. As soon as the returned resultant values are extracted, please take care to de-allocate the memory on the
heap - there is nothing worse than unsanitary memory usage. Please see below for example setup and usage.

<br>

### _Binder_ ![Binder](./img/binder_32px.png)

No modifications are required to the `Binder` to get custom `Client`s and `Server`s to function correctly.

<br>

### _Client_ ![Client](./img/client_32px.png)

To develop a custom `Client` please look at the `client1.c` file located in `extern_test`. A quick example of calling an
RPC which adds two values is as follows:

```c++
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "../src/rpc.h"             // Must be included to access <RPCLib> routines and constants.

int main(int argc, char *argv[])
{
    //- Variable Declaration(s):
    int arg_1 = 5;                  // First argument for remote procedure call.
    int arg_2 = 10;                 // Second argument for remote procedure call.
    int retval;                     // Procedure return value from remote procedure call.
    int arg_cnt = 3;                // Count of arguments including the return value.
    int argTypes[arg_cnt + 1];      // Argument type array counter. Includes terminal null.
    void **args_add;                // Argument memory on heap.

    // Pack the argument type container with specifications of the arguments.
    argTypes[0] = (1 << ARG_OUTPUT) | (ARG_INT << 16);          // Return value.
    argTypes[1] = (1 << ARG_INPUT) | (ARG_INT << 16);           // Argument 1: <arg_1>
    argTypes[2] = (1 << ARG_INPUT) | (ARG_INT << 16);           // Argument 2: <arg_2>
    argTypes[3] = 0;                                            // Terminal <null>.

    // Allocate memory on the heap and pack the argument array for the RPC call.
    args_add = (void **) malloc( arg_cnt * sizeof(void *) );
    args_add[0] = (void *) &retval;
    args_add[1] = (void *) &arg_1;
    args_add[2] = (void *) &arg_2;

    // Initiate RPC call.
    int retcode = rpcCall("func_add", argTypes, args_add);

    // Test response for correctness.
    printf("\nEXPECTED return of func_add is: %d\n", arg_1 + arg_2);
    if (retcode >= 0)
    {
        printf("ACTUAL return of func_add is: %d\n", *( (int *) (args_add[0]) ) );
    }
    else
    {
        printf("ERROR: %d\n", retcode);
    }

    // Don't be unsanitary by pooping on the heap.
    free(args_add);

    return 0;
}
```

<br>

### _Server_ ![Server](./img/server_32px.png)

Setting up a `Server` is split into two major steps, the first of which is setting up the executing `Server` and the
second of which is registering the `Server` with the `Binder`. The second setup is rather straight forward so we shall
begin with assembling an executing `Server` first.

<br>

We begin by setting up the function skeletons which will be used to un-marshall the request arguments from the inbound
data off the wire. Then we setup the actual routine which will be executed by the `Server` servicing the requests:

```c++
// server_functions.h -- Function prototype for routine which will execute on the data.
int func_add(int arg1, int arg2);   // return: OUT; arg1, arg2: IN
```

```c++
// server_functions.c -- Routine which will actually execute on the data.
// return: OUT; arg1, arg2: IN
int func_add(int arg1, int arg2)
{
    return arg1 + arg2;
}
```

```c++
// server_function_skels.h -- Used to un-marshall parameters and call the executing function.
int func_add_skel(int* arg1, void** arg2);
```

```c++
// server_function_skels.c -- Used to un-marshall parameters and call the executing function.
#include "server_functions.h

int func_add_skel(int *argTypes, void **args)
{
    *(int *)args[0] = func_add(*(int *) args[1], *(int *) args[2]);
    return 0;
}
```

<br>

We can now proceed to setup the actual `Server` which will service the execution requests:

```c++
#include "../src/rpc.h"
#include "server_function_skels.h"

int main(int argc, char *argv[])
{
    // Prepare <Server> function signature.
    int arg_cnt = 3;
    int argTypes[arg_cnt + 1];

    argTypes[0] = (1 << ARG_OUTPUT) | (ARG_INT << 16);
    argTypes[1] = (1 << ARG_INPUT) | (ARG_INT << 16);
    argTypes[2] = (1 << ARG_INPUT) | (ARG_INT << 16);
    argTypes[3] = 0;

    // Open a connection to the <RPCLib> <Binder>.
    rpcInit();

    // Register the addition function with the <Binder> for service.
    rpcRegister("func_add", argTypes, *func_add_skel);

    // Put the <Server> into service by putting it in an executing state. This is a blocking call.
    rpcExecute();


    // On return from the prior blocking call the application exits.
    return 0;
}
```

<br>
<br>
<br>

---

###### Copyright and Legal Notice(s):
&copy; Saad Ur Rahman. All rights reserved. Usage and source code is licensed under the
`GNU Affero General Public License v3.0`.

Icons made by [Freepik](https://www.freepik.com/ "Freepik") from
[www.flaticon.com](https://www.flaticon.com/ "Flaticon")
is licensed by [CC 3.0 BY](http://creativecommons.org/licenses/by/3.0/ "Creative Commons BY 3.0").

---
